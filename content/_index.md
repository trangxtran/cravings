---
preview: media/hero.jpg
align: right
title: Trang's Cravings
description: >
  Come discover all the books, restaurants and spots that I always come back
  to. You are about to enter my personal realm of food and delicacies:
  feel free to venture in.
---

## Latest Posts

*“Let food by thy medicine and medicine be thy food.”
— Hippocrates*
