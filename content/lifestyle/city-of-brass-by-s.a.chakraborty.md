---
categories:
- Books
date: 2020-08-01T04:00:00.000+00:00
position: []
title: City of Brass by S. A. Chakraborty
preview: "media/img_9199.jpg"
---
{{% content %}}
RATING: 4.5/5

I took so much time in reviewing this book but that only because it’s too good of a book to even find the right words to describe but I will try to make it happen. In short, before reading this book, I had very few knowledge about Arabian Mythology, most of my knowledge comes from One Thousand and One Nights book. But oh my, have I been introduced to one of the most diverse mythology world that existed.

![The City of Brass book cover](media/img_9199.jpg)

**The last time I read about a world building this rich and mesmerizing would be in** [**Strange The Dreamer**](http://bookidote.com/2017/05/06/strange-the-dreamer-by-laini-taylor/)**and** [**The Night Circus**](http://bookidote.com/2016/04/09/the-night-circus-by-erin-morgenstern/)**.** For a debut novel, **the author totally nailed it.** S.A. Chakraborty is a devoted Middle Eastern history and researcher. There are a few books that I read and I can see the enthusiasm and the passion that burns through their ideas and Chakraborty has made it clear that she loves what she is writing about. The detailed world she’s building is inspired by many many years of Middle Eastern, Indian and African history which I have learned a great tons about.

![The City of Brass book cover in a backpack](media/img_9200.jpg)

What I love about this book is everyone in this story is doing what they do because they think it’s the right thing to do, in each of their unique ways to fight for their own version of what is right. I personally never found myself in a situation where I would root for.. everyone. **With a solid world building, and a magical system that makes sense, the only complaint I have is how confusing it was for me to figure out which tribe is which and what tribe did what to the other tribes haha.** The pace was absolutely phenomenal, the actions were at the right time, some slowing moments for us to reflect on the character’s past and motivations and all the cliffhangers and the twist and turns in the end.

I personally cannot wait to start the sequel as the third book of the series got released in store 3 weeks ago! Adding this book to my never ending list of to-be-read haha.

That's it for today guys! See you in the next book review ;)
{{% /content %}}
{{% float %}}
**Series**: [The Daevabad Trilogy #1](https://www.goodreads.com/series/211584-the-daevabad-trilogy)
**Author**: [S.A. Chakraborty](https://www.goodreads.com/author/show/16002992.S_A_Chakraborty)
**Publisher:** Harper Voyager
**Release:** November 14th 2017
**Genre:** Fantasy
**Hardcover:** 544 pages
  {{% /float %}}
