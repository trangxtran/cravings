---
categories:
- travel
date: 2022-07-24T00:00:00-04:00
title: 'Dublin : Where to Eat and Drink'
preview: "media/draper4.JPG"
---
{{% content %}}

Last June, I got the opportunity to present at a conference with my work lab and since my fiancé was also on vacation, we took the opportunity to discover a bit more of Dublin, Ireland and the food they offered.

[**Soup**](https://g.page/Soup-2?share)

The amazing selection of dashi and Ramen of this place, blending with its savors in Japan and Dublin, like pickled eggs and dashi broth. On top of that, the placeis super cute! I highly recommend it if you are looking for a little lunch place.

![](media/soupesoupe.JPG)

[**Yamamori Izakaya**](https://g.page/yamamori-izakaya-sake-bar?share)

The best meal of our entire trip in Dublin, is this Izakaya. I know you must be saying, it's Japanese food in Dublin?! But yes yes. The whole menu is looking delicious and once we tried the Toro, the foie gras temaki, my mind just went bezerk. The cocktails were also very good, the Lychee cocktail was my favourite.

![](media/izkaya1.JPG)![](media/izakaya5.JPG)![](media/izakaya4.JPG)![](media/izakaya3.JPG)

[**Farrier & Draper**](https://www.farrieranddraper.ie/)

One of the most stylish bars in Dublin, no doubt. They offer an extensive range of spirits and we tried 2 of their cocktails and one of my favourite is the Pink Satin : Elderflower, Gin, Raspberry syrup, Lemon Juice, Whites and Tonic bitters. As for the food, we had parmesan fried with truffle oil, the meatballs and the fettucine with ragu sauce.

![](media/draper4.JPG)

![](media/draper3.JPG)

![](media/draper1.JPG)

[**Idlewild**](https://g.page/idlewilddublin?share)

A very cool bar, with a big fireplace and lounge couches and love the attentive staff. We went to the bar on a late night and it seemed like most people don't go out too late so it was a bit empty. Nonetheles, we had a great time and the drinks all the while delicious!

![](media/bar2.JPG)

![](media/bar.JPG)

[**Tram Cafe**](https://www.thetramcafe.com/)

This coffee shop inside of a real historic Tram makes the morning coffee even more charming! They sell crepes, breakfast items and the hot chocolate was tasty. The location is also a big 10/10 for me. Situated in a beautiful park, you get a nice view and a very relaxing atmosphere.

![](media/tramcafe.JPG)

![](media/img_9779-2.JPG)

![](media/img_9725.JPG)

{{% /content %}}

{{% float %}}

In this post:

1- Soup

2- Yamamori Izakaya

3- Farrier & Draper

4- Idlewild Bar

5- Tram Cafe 

{{% /float %}}
