---
categories: []
date: 2022-05-26T00:00:00-04:00
title: 'WHAT TO DO IN PHILADELPHIA'
preview: "media/img_8475.JPG"
---
{{% content %}}

I had the opportunity to do a short trip in Philadelphia fora neurosurgery conference and during the span of 3 days I tried as much to visit the city. Here are some of my recommendations. I selected one spot per type!

## Where to eat

The food scene in Philadelphia is much better than I expected and for the same quality, it's way cheaper than NYC!

### Double Knot

**My biggest highlight** is definitely [Double Knot](https://www.doubleknotphilly.com/downstairs), a magnificent bar when you come in but the real treasure is in. the speakeasy kitchen downstairs serving Japanese Fusion menu. My favourite item on the menu is the **Edamame Dumpling** (sake,pea shoot, truffle) served in a broth. I had to order 2! That's how much I loved it.

![](media/img_7366.JPG)
![](media/img_7556.JPG)
![](media/img_7555.JPG)

## Where to chill in a coffee shop

**Menagerie Coffee** is probably the cutest coffee shop I encountered in Philadelphia. Located in the beautiful neighborhood Old City, Menagerie Coffee offers a nice little touch of color and a retro vibe at the same time.

![](media/img_8482.JPG)
![](media/13615785-d2b2-4348-a7bd-359a49e3a506.JPG)
![](media/bca523fa-05ca-470d-8d0d-a5c4fae9d22c.JPG)
![](media/3866dd39-4cd2-4946-9d64-4d59c09ad0ea.JPG)

## What to visit

### The Independence Hall

An important historical structure where the Declaration of Independence was signed. You can pay for a guided tour inside too.

![](media/img_8470.JPG)
![](media/img_7434.JPG)

### The Mütter Museum at The College of Physicians of Philadelphia

A bit of an unusual museum but so impressive! If you want to peek your curiosity at the humain anatomy, this museum gathers more than 20k objects including Einstein's brain slides!

### Eastern State Penitentiary

Al Capone's cell and first penitentiary of its kind in the US. It's a refreshing experience in a sociology and psychology perspective. It was revolutionary at the time because it's the first system to encouraged separate confinement as a form of rehabilitation.

{{% /content %}}

{{% float %}}

In this post:

1. Where to eat
2. Where to chill in a coffee shop
3. What to visit

{{% /float %}}
