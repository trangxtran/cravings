---
categories:
- France
- Travel
- Marseille
- Nice
- Michelin Star
date: 2020-07-22T04:00:00.000+00:00
position: []
title: French Riviera in 4 days
preview: "media/lrm_export_99368372481208_20190815_123844678-1.jpeg"
---
{{% content %}}
> [_Original post on Bookidote_](https://bookidote.com/2019/08/22/french-riviera-in-4-days/)

Back in July 2019, I had the chance to go to Nice thanks to a [food blogging opportunity](https://trangxtran.com/) : they would cover all the flight tickets to the hotel for me and **I could bring a plus one, so I chose to bring my mom!** Most of you know how she had a hard time last year with her condition and I figure this was an amazing time for her to recover and enjoy life again. That’s where our adventure to Nice begins!

## DAY 1 – MARSEILLE

We landed in Marseille and had a few hours of free time before our train to Nice, so we decided to take a toll in their Old Port. It was super hot, record breaking hot in Marseille. **37 to 40 degrees celsius**! I was boiling. But the view is very pretty and the food was delicious. Like [**Anthony Bourdain**](https://www.cnn.com/travel/article/marseille-bourdain-journal-parts-unknown/index.html) said : _“Maybe it’s because the French don’t seem to want you to go to Marseille, and what they mean by that is that it’s too Arab, too Italian, too Corsican, too mixed up with foreignness to be truly and adequately French.. **It is a glorious stew of a city, smelling of Middle Eastern spices, garlic, saffron and the sea.”**_

I approve 100%, as the second largest city in France **it is charming in every single way,** I can eat one of the best gelato as well as eat a delightful bouillabaisse. If you go there as a curious food lover, you will be satisfied.

![Inside a turquoise cafe](media/french-riviera-1.jpeg)
![Busy streets of Marseille](media/marseille.jpeg)
![Mom by the docks](media/marseille1.jpeg)

## DAY 2 – NICE (DAY) – MONACO (NIGHT)

We started the day with a delicious French breakfast prepared by Emma, a local foodie, the table was garnished with amazing smells and flavours: from lavender yogurt, to buttery croissants, you can find everything. [Click here for a preview of that beautiful banquet. ](https://videopress.com/v/dxLaYF3Y)

![Our balcony with a street view](media/lrm_export_410456262597953_20190707_150713290.jpeg)
![A cute street intersection](media/lrm_export_99368372481208_20190815_123844678-1.jpeg)

You can’t go to Nice without trying their beaches, one of my favourite waters : clear and super clean. No sands though. LOL Be prepared to bring on some sandals in the water.

![The amazing waters of the beach](media/lrm_export_410451761091548_20190707_150708789.jpeg)
![The beach view](media/lrm_export_411024081358309_20190707_151641109.jpeg)

We took the train to **Monaco at night** because we heard it’s more charming and enchanting. But to be honest, **Monaco was the most disappointing city of all haha.** The city may be rich but it’s so empty practically nothing to do of you’re not into the Casino and Party vibes. My Mom and me, we are far from that type of crowd haha.

![Monaco at night](media/img_20190707_221652.jpg)

## DAY 3 – NICE - CANNES

We ate breakfast at the most beautiful rooftop terrace restaurant in Nice thanks to my food blogging sponsoring, the food was delicious and the view is to die for.

_LE MERIDIEN – NICE_

![Appetizers with a view](media/66632720_458321751390548_482535774659543040_n.jpg)
![Cocktails with a view](media/66692892_458321721390551_7419901439469682688_n.jpg)
![Even more appetizers](media/66245917_458321684723888_5309885724655353856_n.jpg)

Around 2PM, we headed to **Cannes** ! The city of Fashion and we couldn’t wait to see their beaches either.

![Sun tanning](media/2019-07-08-18-15-08.jpg)
![Mom visiting Cannes](media/img_20190708_163636.jpg)

Also as part of my food blogging tour, I had the chance to dine at the One Michelin Star restaurant in Cannes, feeling very lucky to be able to share this meal with my Mom ! See my Instagram post for the food details:

## DAY 4 – NICE

Last day in Nice and we went on a snorkelling adventure! It was my first time and I was very scared because although I can swim, I can’t float. I learned it and somehow I just can’t LOL This experience was so cool, we get to see the little fish and the water was so clear.

![Going snorkeling](media/img-7e830044c03ce16c6c84c37cd62b8c06-v.jpg)

Before heading home, we wanted to make one last stop to eat some truffle and pasta, truffle is one of the specialties in Nice and I had to come taste it.

![Sipping cocktails](media/img-1ed4ebdc8319c1cf35c98c59c04cac4d-v.jpg)
{{% /content %}}
{{% float %}}
**Day 1** Marseille

**Day 2** Nice - Monaco

**Day 3** Nice - Cannes
 {{% /float %}}
