---
categories:
- Lifestyle
date: 2020-11-04T00:00:00.000-05:00
title: Meemoza Fall Collection
preview: "media/img_8544-2.JPG"
---
{{% content %}}

Montreal has been a long time one of the most renowned Fashion scene in the world and during this pandemic, I feel the need to emphasize and support the local business even more. That's when I discovered [Meemoza](https://en.meemoza.ca/), a brand that has eco-friendly values in mind and features cozy yet chic pieces that are implant themselves as classic items.

_Pants from Meemoza - photo by @_[_artsyvisuals_](https://www.instagram.com/artsyvisuals/?hl=en)

![](media/img_4947.JPG "Pants from Meemoza - photo by @artsyvisuals")

The beige pants are comfortable but also a very smooth fabric that embodies your shape and yet let you move around.

![](media/capture-d-ecran-le-2020-11-04-a-22-10-00.JPG)

_This picture and the following are actually a snippet of a Fall Lookbook video I did in collaboration with_ [_@artsyvisuals_](https://www.instagram.com/artsyvisuals/?hl=en)_, check it out_ [_here_](https://www.instagram.com/trangreeny/channel/)_._

![](media/capture-d-ecran-le-2020-11-04-a-22-09-43.JPG "Pants from Meemoza - photo by @artsyvisuals")

Another piece that I love from their Fall collection is the [Lou Wool Skirt](https://en.meemoza.ca/collections/skirts/products/jupe-lou-laine-ah19), paired greatly with that Back To School outfit, a white dresshirt and a satchel bag.

_Skirt from Meemoza - photo by @_[_trangreeny_](http://instagram.com/trangreeny)

![](media/img_8544-2.JPG)

Up next, one of my favourite items of the year is the Warhol Pants. They give out the 60s vibes but also let you rock a nice carmine burgundy color. I also love to pair it with their chemiser with a cute polka dots pattern.

_Pants and Blouse from Meemoza - video by @_[_artsyvisuals_](https://www.instagram.com/artsyvisuals/?hl=en)

![](media/capture-d-ecran-le-2020-11-04-a-22-08-42.JPG "Outfit from Meemoza - photo by @artsyvisuals")

![](media/capture-d-ecran-le-2020-11-04-a-22-08-59.JPG "Outfit from Meemoza - photo by @artsyvisuals")

The versatility of these pants make it an absolute must item in my opinions.

_Pants from Meemoza - photo by @_[_artsyvisuals_](https://www.instagram.com/artsyvisuals/?hl=en)

![](media/img_5325-2.JPG "Pants from Meemoza - photo by @artsyvisuals")

**Big thank you to Meemoza for gifting me the beautiful items!**

{{% /content %}}

{{% float %}}

# Meemoza

[Website](https://en.meemoza.ca/) --- [Address](https://meemoza.ca/pages/visit-the-shop)

**Rating:** 5/5

{{% /float %}}
