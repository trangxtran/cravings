---
categories:
- travel
date: 2022-03-22T00:00:00-04:00
title: 'TOP SPOTS IN NEW YORK 2022 '
preview: "media/img_6869.JPG"
---
{{% content %}}

This trip was probably one of my favourite NYC trips of all times, I've been back to this city a couple of times and I think I'm at a place in my life right now where I can fully enjoy New York. It makes a whole difference!

Here's a list of **my favourite spots** from my last trip to New York :

#### **RESTAURANTS**

**LILIA**: If you're fan of pasta and wine bar, this is the place for you. Situated in Brooklyn, in a charming neighborhood, Lilya has kindly allow our group of 4 for a walk-in and I will forever be grateful!

![](media/e3cf57cd-3afb-43cc-b64c-ca2c160e8187.JPG)

![](media/img_6884.JPG)

**TUOME**: A Michelin-star restaurant who totally delivered their whole menu! My favourite is their classic snow crab in dashi butter dish. Also, the absolute best and cook to perfection octopus we had for life. You will be thinking about it for **days**.

![](media/img_6956.JPG)

![](media/img_6953.JPG)

#### **SPEAKEASY BARS**

**THYME BAR:** My favourite speakeasy bar, for sure! It's located under a little pastry and the interior design is just so beautiful. Adding to it, the drinks are work of art and for all budget! The cocktails we tried were solid and have a lot of different and interesting flavors all together. Definitely, an experience to have if you want to treat yourself a bit!

![](media/img_4887.JPG)

![](media/img_4891.JPG)

**PDT (Please Don't Tell) :** Psss. _I'm sorry for revealing you guy's name !_ But this speakeasy is such an institution that could not not pass by it when you're in New York. It's very quaint and relaxed atmosphere but the coolest part? You have to enter the phone booth to get it!

![](media/img_4772.JPG)

![](media/img_4776.JPG)

#### **COFFEE SHOPS**

**ppl (Brooklyn)**: Plants, plants and greenery all round this cute coffee shop! No sit-in but a takeout experience like you've never seen before. It's a little local business filled with little vintage objects all around speaking of its history. I love the details surrounding this place: books, pots, glass tubes.

![](media/img_4807-2.JPG)

![](media/img_6968.JPG)

**Felix Roasting Co.:** Very much instagram-worthy place with its beautiful pink and blue decor. But what's even more worthy is their mascarpone and figue toast. A delicious snack for our trip!

![](media/img_4712-2.JPG)

It was hard to filter out the top spots but I think this is the most concise list I can think of. Stay tuned for the next traveling post!

(c) All pictures by me

{{% /content %}}

{{% float %}}

**MY TOP SPOTS:**

1\.[LILIA](https://www.lilianewyork.com/)

2\.[TUOME](https://www.tuomenyc.com/)

3\.[THYME BAR](https://www.thymebarnyc.com/)

4\.[PDT (Please Don't Tell)](http://www.pdtnyc.com/)

5\. [ppl](https://pplnyc.com/)

6\. [FELIX ROASTING & CO.](https://felixroastingco.com/ "FELIX ROASTING CO.")

{{% /float %}}
