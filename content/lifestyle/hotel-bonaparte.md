---
categories: []
date: 2021-08-26T00:00:00-04:00
title: 'Hotel Bonaparte '
preview: "media/img_9839.JPG"
---
{{% content %}}

Once in a while, you fall in a hotel room that will mark you for life. And that's what **Hotel Bonaparte** is to me. Situated in the heart of Old-Montreal, and with the balcony that is above the courtyard of Basilica Notre-Dame, the room that we got might be a bit smaller than usual but the balcony is spacious and a dedication to those Parisian balconies but with a magnificent view.

![](media/img_9825.JPG)

The staff were lovely and when I randomly asked for a wine glass at 11pm, they did not complain and still serve me with the biggest smile. If you want a little Parisian atmosphere in Montreal, I definitely you go all out with this room and additional balcony.

Also make sure to try out their restaurant Bonaparte and pass by LAPop for some delicious croffles ! ;)

![](media/img_9803.JPG)

![](media/img_9856.JPG)

{{% /content %}}

{{% float %}}

**Hotel Bonaparte :** [**Website**](http://bonaparte.com/en/)

Rating : 4/5

{{% /float %}}
