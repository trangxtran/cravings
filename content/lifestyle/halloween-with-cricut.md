---
categories: []
date: 2021-10-30T00:00:00-04:00
title: Halloween with Cricut
preview: "media/cricut_setup.JPG"
---
{{% content %}}

When Cricut challenged me for an opportunity to create some DIYs for Halloween, I knew exactly what I wanted to do. As many of you know, I've been a fierce lover of the dark academia aesthetics and **what goes more with that than a witchcraft setup for the Halloween?**

![](media/cricut_setup.JPG)

The first thing that a witch can never go without is a spell.. so I made a **Spell Jar** sticker with Gothic font and the **Cricut Joy** printed it out. I stick it to an old glass jar I have around the house.

![](media/spell_jar.JPG)

After the jar, I originally wanted to make a Spell Book but with a few time constraints I made a **Time Spell Parchment Roll i**nstead! I wrote **Tempus Edas Rerum** (which means _Time, Devourer of Everything_ in Latin) and the Cricut Joy wrote it on the Parchment. I also added **_Veni,Vidi,Vici_** on a candle label to add to the dark look with the parchment.

![](media/parchment-paper.JPG)

And of course, who says witches without spirits? I printed out some cute **little ghosts** and put them on the pumpkin and also made a card **_To My Boo_**, which is a premade model on the Cricut App. You can have a 30 days free trial on the App that allows you to have access to all of their drawings and premade models and templates.

![](media/cricut_pumpkin.JPG)

![](media/cricut_card.JPG)

As my first time owning a Cricut machine, I think the experience was quite fun but I will be lying if I say it wasn't challenging. The smaller parts are trickier to peel off, but Cricut also offered tools that help the process. The one I received is the **Cricut Joy** and I love how small and compact it is, it doesn't take a lot of space and the possibilities are quite endless with this little gem for my DIYs project. But like any DIYs project, all it takes is a bit of patience and the joy of creating!

![](media/cricut_joy.JPG)

_Thank you to Cricut Canada for sending me the Cricut Joy as part of this collaboration._ 

{{% /content %}}

{{% float %}}

Cricut is currently available from Canadian retailers such as [Amazon](https://www.amazon.ca/stores/Cricut/page/1B1CE103-125D-4C7F-86B6-F5E6CB9C59B1?ref_=ast_bln), [Costco](https://www.costco.ca/CatalogSearch?dept=All&keyword=cricut), [Indigo](https://www.chapters.indigo.ca/en-ca/home/brand/cricut/), [Michaels](https://canada.michaels.com/en/cricut), [Staples](https://www.staples.ca/search?query=cricut) and [Walmart](https://www.walmart.ca/brand/cricut/51013763?icid=Search_wmg_display_walmart_SBS_wk25_cricut_en).

{{% /float %}}
