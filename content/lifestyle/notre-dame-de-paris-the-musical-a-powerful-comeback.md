---
categories:
- culture
- musical
date: 2022-08-13T00:00:00-04:00
title: 'NOTRE-DAME-DE-PARIS THE MUSICAL : A POWERFUL COMEBACK '
preview: "media/img_3227-2.JPG"
---
{{% content %}}

**_Notre-Dame-de-Paris_** was probably one of my favourite books from Victor Hugo. Growing up in France, reading Victor Hugo was simply a necessity for me. And when I heard the song _Belle_ interpreted by Garou, Patrick Fiori and Daniel Lavoie on Youtube, I knew I had to assist the musical in-person someday.

Fast forward 11 years, the little girl that once dreamed to see the artists in-person has now been able to assist the media premiere of the same piece with the original actors from their first 1998 screening: Bruno Pelletier as Gringoire and Daniel Lavoie as Frollo. What an amazing night! Both actors and singers perform with a professionalism, a maturity and a voice control that you could tell the role has been in their blood for years. _Le temps des Cathédrales_ opened the musical under an a very well deserved explosion of applauds from the public and I'm for sure as for Bruno Pelletier as well, we were all happy to find Gringoire again.

I could feel the little nervousness in the voice of Esmeralda interpreted by belle Elhaida Dani when she started her first solo but she delivered gracefully in the end, getting more comfortable with the public. My only critic would be for Frodo interpreted by Angelo Del Vecchio during the _Belle_ performance, in the end, where the three characters' voice blend in together, Frodo's new version seemed to want to overshadow and put a bit too much of show, too focus on his singing rather than to work as a team with the other characters on scene. But in the end, his interpretation of _Les Cloches_ was mesmerizing and with the dancers doing all the acrobatics, they put on a real show.

Fleur-de-Lys and Phebus did a fantastical job in my opinion as a duet, I could see them as a couple and even more so than Esmeralda and Phebus I dare say.

Overall, I would recommend this musical to everyone because Luc Plamondon and Richard Cocciante did an amazing job at creating one of the best songs in musical history.

![](media/img_3230-2.JPG)

![](media/img_3227-2.JPG)

![](media/img_3216-2.JPG)

{{% /content %}}

{{% float %}}

Notre-Dame-de-Paris the Musical : [website](https://www.tandem.mu/notredamedeparis/#spectacle)

Now performing in Montréal, at Salle Wilfrid-Pelleter in Place Des Arts until August 25, 2022. 

{{% /float %}}
