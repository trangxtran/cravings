---
categories: []
date: 2021-11-01T00:00:00-04:00
title: WHAT TO DO IN VIENNA
preview: "media/img_9132.JPG"
---
{{% content %}}

Thanks to a work opportunity, I had the chance to discover Vienna, Austria recently and I wanted to give a quick tour of what I did in my downtime and what to consider during these COVID times.

The first difference with Montreal, is that if you are fully vaccinated in Europe, you can walk inside without a mask. I weirded out at first but after a week, you do get used to it and it doesn't seem as dangerous. We were tested negative on our way back which means we were pretty safe during our trip.

**For Austria especially, be mindful to always have a FFP2 Mask wherever you go. Some touristic attractions don't let you in if you don't have that specific mask.**

With all of those travel advisories out of the way, let's jump into the fun stuff!

1. **Coffee Shops**

There are so many cute coffee shops in Vienna, if you are big into the coffee crawl culture, you will have a blast here. My top 3 are **Carl Ludwig Cafe**, **Paremi Boulangerie** and **Adlerhof**.
**1.1 Adlerhof :** A beautiful indoor garden, the place has many floors and it's definitely a worthy Instagram place. However, they don't allow professional pictures to be taken inside, only mobile phones pictures allowed.

![](media/adlerhof.png)

**1.2 Paremi Boulangerie :** Definitely my favourite place to get a fresh pastry in the morning! The staff are also super friendly. I got a caramel nutella corbeille, it was one of their specials of the Fall season and it was the best thing I had the whole trip.

![](media/paremi.JPG)

![](media/paremiboulangerie.JPG)

**1.3 Carl Ludwig Cafe :** If you want a coffee with a view, this is it. A cute hidden gem close to University campus. A lot of students come here to study and catch up with friends.

![](media/ludwigcafe.JPG)

1. **Must-see places**

It's easy to get distracted in Vienna, there are so many beautiful architecture and the richness in culture and history that it was hard for me to choose but I had to prioritize in a short-time period.

**2.1 Museum of National History :** I'm a big fan of museums and Vienna seems to be the city to dive in them. This museum is very impressive, it detailed the evolution of Planet Earth in details with more than 100,000 historical finds. Upon entering, you are welcome with the inscription : _to the realm of nature and exploration._

![](media/museumnationalhistory.JPG)

**2.2 Schonbrunn Palmenhaus:** Definitely a must for plant lovers, it is home to more than 45,000 plant species and one of the largest greenhouse of its kind in the world.

![](media/img_3481.JPG)

![](media/img_3478-2.JPG)

![](media/img_8647.JPG)

**2.3 The National Library :** The bookworm in me HAD to visit this beautiful place. It's the country's largest and one of the most important institutions with more than 12 million items in various collections. It's main prestige resides in the particular [8 departments:](https://www.onb.ac.at/en/library/departments) manuscripts and rare books, maps, the papyrus collection,etc.

![](media/national-library.JPG)

That's a little recap for this post, I hope you get the chance to visit this city one day! However, I do advise to be cautious while considering to travel in Europe for leisure. I would prefer to come back for vacation only if some restrictions start to loosen up. I'm easliy anxious and it's just not the kind of stress I liked to deal during the pandemic. The covid tests come with a cost and the constant worry to have a negative result can affect the travel as well.

{{% /content %}}

{{% float %}}

In this post:

1- Coffee Shops in Vienna

2- Must-See Places in Vienna

{{% /float %}}
