---
categories:
- dinner
date: 2022-02-09T00:00:00-05:00
title: TOP ROMANTIC RESTOS IN THE OLD-PORT
preview: "media/barroco.JPG"
---
{{% content %}}

One of my most requested list this week haha! I think these restaurants hit the right spot for me, not only they are in my neighborhood but in terms of a cute little date : perfect ambiance, not too loud and delicious food. Once you're done, you can take a little stroll in the Old-Port for that little romantic walk.

1. [**Tiers Paysage**](https://tierspaysage.com/)

   From the excellent staff and customer experience (always with a big smile from Samia!) to the impeccable wine list and a menu that varies with the freshest ingredients of the season. You will find a chic decor, simple yet elegant and pay tribute to the historical landscape of Montreal.

   ![](media/img_6931.JPG)
2. [**Auberge St-Gabriel**](http://aubergesaint-gabriel.com/en/)

   If you want that cozy cottage feel atmosphere near a fireplace in this winter, this is the place. I have to be honest, I was skeptical of the place at first as I am with most of the restaurants in the Old-Port close to Place Jacques Cartier. But the menu is actually pretty good! Lobster raviole burrata in their recent item or if you fancy some meat, must and herb leg of lamb.

   ![](media/auberge-st-gabriel.JPG)
3. [**Tbsp**](https://www.tbsprestaurant.com/fr/)**.**

   You know what's great about this place? The design is beautiful, clear lines, chic and they're open 7/7. With dimming lights, it makes for a lusting atmosphere inside the W Hotel. My highlights are the Spaghettoni Verde allo scoglio, a seafood pasta dish that was light and yet offered all the savors and the porcetta snacks.

   ![](media/tbsp1.JPG)
4. [**Caffe Un Po Di Piu**](https://www.caffeunpodipiu.com/)

   Whenever I want to treat myself and my fiancé, we go automatically to this restaurant. Not only it has a beautiful view on De La Commune, it also has some amazing special pasta dishes! The design at night can't be more romantic, with little candles on the tables and if you're lucky to ge a table besides the window, it's even more magical.

   ![](media/caffeunpodipiu.JPG)
5. [**Barrocco**](https://www.barroco.ca/)

   Now if you're looking for that typical stone historic and Old-Port atmosphere you'll find it at Barroco. They have such an amazing sense of history, that you can't help but look at the walls and pay attention to the details : the candleholders, the plates and their funny messages, the books on the shelves, every single thing compliments the place. Foodwise, you can never go wrong with their Papardelle and their Paella! Also, make sure to look at the cocktails menu, one of the most extensive menu I've seen.

   ![](media/barroco.JPG)
6. [**Le Club Chasse et Pêche**](https://www.leclubchasseetpeche.com/)

Last but not least, Le Club Chasse et Pêche, a must in the Old-Port if you want to take it up a notch (quality wise but also budget wise!). The romantic lodge setting would make you swoon. And added to that, the amazing service will make your significant other feel like royalty. The dishes paying homage to both hunting and fishing, are not overly heavy and surprisingly light. A fine-dining experience that would remember !

![](media/clubchasseetpeche.JPG)![](media/clubchassetpeche.JPG)

And there you go, hope you enjoy my recommendations and a Happy Valentine's day to everyone out there!

(c) All pictures are taken by me

Trang

{{% /content %}}

{{% float %}}

**MY TOP 6:**

1\.Tiers Paysage

2\.Auberge St Gabriel

3\.Tbsp

4\.Caffe Un Po Di Piu

5\. Barroco

6\. Le Club Chasse et Pêche

{{% /float %}}
