---
categories:
- vancouver
- travel
date: 2022-10-07T00:00:00-04:00
title: 'Vancouver: Where To Eat and Drink'
preview: "media/askforluigi1.JPG"
---
{{% content %}}

Finally got to the West Coast of Canada while attending my dear friend's wedding and I was really excited to try out the restaurant scene in the city. Here are some of my favourite places I went for a drink, get a brunch and have dinner.

## TO EAT

### Bar SuSu

My favourite experience in the city so far, big thanks to Virginie for the wine pairing! The menu might seem small  at first but a delightful selection and we left with our tummy filled. We had the tasting menu and I would recommend anyone going there to get it. It's 55$ per person and the chef's selection has some of the best dishes of the restaurant. Light and yet so rich in savour. The interior is quaint and cozy and honestly reminded me a lot of Montreal.

_209 E 6th Ave, Vancouver, BC V5T 1J7_

![](media/barsusu1.JPG)![](media/bar-susu3.JPG)

### Suika

Japanese food heaven! We had the beautiful bluefin otoro, so delicate and unctuous.Wagyu pressed sushi dipped in an onsen egg, a platter of fresh sashimi, delicious korokke as well. We came out of there full and with a happy belly.

_1626 W Broadway, Vancouver, BC V6J 1X6_

![](media/img_1952.JPG)![](media/img_1948.JPG)

### Raisu

Lunch at Raisu is insane! You can get their limited editions of the bento box, which you have to call 24h prior to order but it's so worth it. Each little dish makes you discover the Japanese cuisine on a whole other level. We also got their seafood meal, delicious fresh seafood of the day. They also have a new drinks menu and my favourite is berry fizz!

_2340 W 4th Ave, Vancouver, BC V6K 1P1_

![](media/raisu1.JPG)![](media/raisu.JPG)

### Nightshade (Vegan)

It was such a pleasure to see McHale in his element again! He and the kitchen staff delivered a solid menu for our night. My favourite is the Yucca Gnudi, Beurre Noisette, Apple, Brussel Sprout Leaves, what a marvel! It makes me think a lot of Bot Chien (Vietnamese dish) but made solely from an italian root. Impressive how cultures can find such similar flavours! Another statement is also the Cappelletti : Sweet Potato Stuffed Pasta, Broccoli Pesto, Parmesan Foam.

_1079 Mainland St, Vancouver, BC V6B 5P9_

![](media/nightshade1.JPG)

### Ask for Luigi

One of the recommendations of Ryan Reynolds himself, this cozy little Italian restaurant delivered in every way! Generous portion of the carbonara, flavorful, creamy and that EGG. I'm obsessed with eggs so it was very important for me to see its texture haha.

_305 Alexander St, Vancouver, BC V6A 1C4_

![](media/askforluigi1.JPG)

### Fat Mao Noodles

For your noodles cravings, this place has one of best broths I've eaten in my life. Their braised duck and noodle soup is heaven on earth.

![](media/fatmaonoodles.JPG)

## TO DRINK

### Is That French

A beautiful wine bar in the Blood Alley, we had their Bubble Flight for 25$ and the decor with the brick walls and brutalism design, makes it a perfect place for a drink. If you can, try their duck truffle pâté as well to accompany the glass of wine.

![](media/isthatfrench.JPG)

### Key Party

A speakeasy behind an accounting office, how clever is that!

![](media/the-key1.JPG)

{{% /content %}}

{{% float %}}

**In this post:**

*To eat:*

1. Bar Susu
2. Raisu
3. Suika
4. Nightshade
5. Fat Mao Noodles

*To drink:*

1. Is That French
2. Key Party

{{% /float %}}
