---
categories: []
date: 2021-10-08T00:00:00-04:00
title: 'No Time To Die : A James Bond Movie '
preview: "media/img_7832.JPG"
---
{{% content %}}

**This is it y'all, the last movie of Daniel Craig's James Bond Franchise.** To celebrate the advance screening, I dressed up the part and **got some cocktails à la James Bond** at the Cloakroom Bar. Isaac Bédard, a talented senior bartender at Cloakroom made 2 cocktails in Bond's honor: Fino Sherry, Amaro Nonino and Cava.

![](media/img_3004-2.JPG)

 The bitterness comes with the angry feeling after a rough mission night and the Cava as a reflection of its bubbly lifestyle. The second is a Martini of course, with an Barrel-Aged Gin. And I have to agree with the others, this movie is probaly the best since **Casino Royale.** All the characters are entertwined and come to an epic conclusion. But what's even better is we get to see all of Bond's vulnerabilities into one movie and how he copes and deals with it. **Great classic opening credit scene and would probably enter one of my favourite intros.** Random fact **but I had the chance to watch the movie as an advance screening at IMAX, and it didn't disappoint, knowing it's the first time an 007 movie has a 40 minutes shot with IMAX cameras, expanded ratio.**

![](media/img_3008.JPG)

**The movie by itself is amazingly directed by Cary Fukunaga who gives it a slick look, not overcomplicating the plot and wrap up everything nicely for the fans.** We get to see Bond at the end of his career, getting back to what really matters the most. My only two critics for this movie is that Ana de Armas could use more screen time. The only scene she appears in, completely stole the show for me. The introduction of the new 007 also gives the franchise the opportunity to follow its adventure without relating it to James Bond, which would make more sense to me. The second critic is the duration of 2h45. It's a long movie and whether I like it or not, I might be getting old here because past 2h and my eyes start to drift away and my eyelids feel heavy. I have to admit I unintentionally fell asleep in a few scenes.

Overall, a solid ending, a worthy and very satisfying conclusion to this epic 15-year run.

![](media/img_7832.JPG)

{{% /content %}}

{{% float %}}

**No Time To Die**

[Trailer ](https://www.youtube.com/watch?v=BIhNsAtPbPI&ab_channel=JamesBond007)

**Rating : 4/5**

{{% /float %}}
