---
categories: ["Books"]
date: 2018-12-11T05:00:00Z
position: []
title: "Hyperion by Dan Simmons"
preview: "media/766d5e58-6f2c-4b86-9254-80742a59d21c.jpg"
---
{{% content %}}
RATING: 4.7/5

> Once in a while, you stumble on a very **unusual** book, **totally out of your comfort zone**, you give it a try and 90% you end up hating it, the rare 10% you end up FRKN LOVING IT. That’s what happened with Hyperion. **This book blew me away.**

#### THE PREMISE

> “It occurs to me that our survival may depend upon our talking to one another.”

With this quote, Dan Simmons has set up the whole novel. Hyperion is a bunch of stories inside a story. **The core of the novel is a pilgrimage to this mysterious planet Hyperion by seven people who seek answers from this _so-called monster/Lord-of-the pain_ The Shrike.** The whole novel is seven stories telling from their point of view of the reason they have come to this pilgrim. The 7 people also fit with 7 stereotypes : **the Priest, the Poet, the Consul, the Soldier, the Detective, the Scholar and the Templar.** This is why it’s an intesting mix. It feels like a short story collections hidden inside a bigger story of the novel.

![](media/766d5e58-6f2c-4b86-9254-80742a59d21c.jpg)

#### THE 7 TALES

My favourite story is the one about **Father Dure told by The Priest**, because Dure _kind_ _of_ reminds me of my Dad. He’s a religious but also has a big curiosity for science and he has been a post-doc researcher for a long time. Father Dure has the same fascination towards science and academics but also battles with keeping his Faith while confronting some disturbing truths. **His journey was one of the finest horror-meets-suspense tale I’ve ever experienced. With the same haunting feeling as Annihilation by Jeff Vandermeer, Dan Simmons creates an atmosphere of confusion and terror.**

> ##### **“IN SUCH SECONDS OF DECISION ENTIRE FUTURES ARE MADE.”**

**I feel like each tale embraces a particular feeling and each of them will impact different types of reader**. **The Scholar**‘s story made me cry like The Niagara Falls, **the Poet**‘s story is a crowd favourite (from what I read from the other reviews) because he’s such a funny and unique character, the **Detective**‘s story is _Altered Carbon_ with a more poetic twist**, the Soldier** mixes a lot of action and so on.

##### TITLE: [HYPERION](https://www.goodreads.com/book/show/36073085-hyperion)

{{% /content %}}
{{% float %}}
**Series:** [Hyperion Cantos #1](https://www.goodreads.com/series/40461-hyperion-cantos)
**Author:** [Dan Simmons](https://www.goodreads.com/author/show/2687.Dan_Simmons)
**Publisher:** Spectra
**Release:** January 12th 2011
**Genre:** Sci-fi
**Paperback:** 481 pages
{{% /float %}}
