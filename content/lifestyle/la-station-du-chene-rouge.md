---
categories:
- Travel
date: 2020-08-18T04:00:00.000+00:00
position: []
title: La Station Du Chêne Rouge
preview: "media/img_2677.JPG"
---
{{% content %}}
![Bedside view inside the cabin](media/img_2677.JPG)
![Eco-lodge sink and mini-furnace](media/img_2603.JPG)

I needed some time off out of town and I was immensely craving for some nature, when I read about [**La Station du Chêne rouge**](https://www.lastationduchenerouge.com/) **I knew right away that's what I needed.**

**We arrived at the eco-lodge after 2 hours drive from Montreal and a very nice welcome from Hilda,** she showed us around the place and explain the outdoors activity we could do during our stay. After that, she kindly leads us to our eco-lodge : L'Air.

[L'Air](https://www.lastationduchenerouge.com/en/ecolodge/rectangles/lair/) is a rectangle shaped lodge just 1 minute from our parking spot which is very convenient to bring all our stuff inside. One side is a beautiful valley field, it's the view from our balcony (yes we had a balcony!) and the other side is the forest view.

![Amazing view from the balcony onto the fields](media/img_2275.JPG)
![Fern forest photography](media/img_2331.JPG)

> _"The unit comes with two Adirondack chairs around a fire pit, a picnic table and a coffee table with two exterior chairs on the balcony." -_ [_La Station Du Chêne Rouge_](https://www.lastationduchenerouge.com/en/ecolodge/rectangles/lair/)

**During the COVID-19 situation, make sure to bring your own bedlinen, pillows, ustensils, pots and cookware. It's also a pet friendly place, for those of you who want to bring their little companion ;)**

Our day of activities begin after we dropped off our stuff inside the lodge. We took some time to admire the beautiful scenery, take some pictures and we were heading for the hiking trails. You have 3 hiking trails, each with different levels of difficulty. As first time hikers, my boyfriend had the brilliant idea to do the most difficult one, the red one. After 2 hours of suffering, and climbing and my legs have given up on me, we finally arrived to the view of a big ruin and a cave haha. It felt very eerie and spiritual stepping in the ruins. Definitely worth the experience.

![Hiking trail](media/img_2490.JPG)
![Abandonned ruins](media/img_2543.JPG)

On our way back, it was getting darker and we had to start preparing our supper. We lit the fire and started our barbecue, we grilled some hamburgers buns and pattys, sliced some tomatoes and grilled some potatoes on the side as well. As for desserts, we ate some pastries that we bought from Cafe VSL (big thank you to Tai!).

![Campfire](media/img_2660.JPG)
![Picnic table](media/img_2611.JPG)

We stay for the night and had to go back in the morning since we had some duties to attend to. If I could, I would definitely spend more than a night here.

_Big thank you to the staff of La Station Du Chêne Rouge for this beautiful collaboration and experience !_

 {{% /content %}}

 {{% float %}} 
# La Station Du Chêne Rouge
 [Website](https://www.lastationduchenerouge.com/en/) --- [Address](https://g.page/lastationduchenerouge?)

**Rating:** 5/5

*Bonus points for the balcony with a view on the stars. We spotted a few shooting stars that night!*
{{% /float %}}
