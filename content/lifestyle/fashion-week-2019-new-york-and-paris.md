---
categories:
- Fashion
date: 2019-10-16T04:00:00.000+00:00
position: []
title: Paris Fashion Week 2019
preview: "media/img_2009.JPG"
---
{{% content %}}
The craziest weeks of my life are the Fashion Week, an unique experience that I had the chance to assist thanks to my friends who were kind enough to bring me along and put on a good word for me (thank you Alicia, if you're reading this).

![Pink satin dress](media/lrm_export_239498445460822_20191002_012906199.JPG)

It is kind of weird to write about this experience a whole year after haha but I will share with you some of my highlights. First highlight is definitely the Chanel runway, I landed in Paris at 8:30AM and the show started at 10am. Needless to say I ran for my life to be there. Arriving, we see the beautiful runway set up on a rooftop and I also spotted Jennie from Blackpink T_T YES.

![Sitting in a camel coat over a white turtleneck in front of the Eiffel](media/img_2023.JPG)

Except for that event, all the other runways seem dull in comparison haha. Just kidding, Louis Vuitton has ended the day with a boom, a magnificent artistic venue.

The most memorable aspect of Paris Fashion Week, is the photographers. Photographers were EVERYWHERE. You would just walk in the street and they would all go crazy and take pictures of you. However, I met one photographer that was so sweet and nice, and he took amazing pictures during my time in Paris.

![Walking in a camel coat over a white turtleneck in front of the Eiffel](media/img_2009.JPG)
![All white outfit under a bridge](media/lrm_export_443335153669210_20191004_223900195.JPG)
![Matching white heeled boots in front of the Eiffel](media/lrm_export_443333580395981_20191004_223858621.JPG)
{{% /content %}}
{{% float %}}
# Paris Fashion Week Runways

Chanel --- Louis Vuitton --- Lacoste

*Dress from [Nastygal](https://www.nastygal.com/ca/)*
 {{% /float %}}
