---
categories:
- Books
date: 2021-05-01T00:00:00.000-04:00
title: Shadow and Bone TV Show Review
preview: "media/img_6609-2.jpg"
---
{{% content %}}

I'M SOO EXCITED TO REVIEW THIS SHOW! :D I have waited 6 years for this and omg guys I'm so happy it's out! You can read my books review of the [Shadow and Bone Trilogy](https://bookidote.com/2016/07/24/shadow-and-bone-the-grisha-trilogy-review/) back in Bookidote earlier days haha. Honest TV show review for this one ? I FRKN LOVE IT!

![](https://bookidote.files.wordpress.com/2021/04/img_6521.jpg)

For those who's not familiar with The Grisha Universe, the Netflix show is a mix between **Shadow and Bone** and **Six Of Crows** from the same author Leigh Bardugo. Alina Starkov  is an orphan and with her childhood friend, Mal, they got recruited as mapmakers and trackers on a dangerous expedition, during which, Alina will discover her true potential.</strong> In parallel, we meet the Crows, a squad of criminals who will try to find their shares of things in this fantastic worldbuilding of Grisha's magic, humans and a struggle for power.

![](https://bookidote.files.wordpress.com/2021/04/img_6609.jpg)

As a die hard fan of the Grisha Universe, I was very ver very happy that they do it justice. **First, they ABSOLUTELY NAILED THE CAST.** Every single character understood their roles, I wonder if they've all read the books because it's an amazing ensemble. **The main character Alina played by Jessie Mei Li totally owns her role, a confused, but still very witty and on her guard type of woman who meets the Darkling (played by Ben Barnes and gosh I couldn't be happier that it was him haha**) and their chemistry on screen is juste insane. Mal also starts to grow on me as I go along and I feel like his character is much more likeable in the show than in the books. But the real screen stealer for me were Jesper, Nina,Matthias and Jesper embodies the funny stereotype to perfection and Nina and Matthias storyline just makes me ship them even more.

**Worldbuilding wise I think Netlfix put the right budget for this show, from the Keftas to the magical displays, to Kaz perfect outfit,  everything was on point.** The Fold as well, as this mysterious entity was represented with such darkness and the CGI of the creatures is in my opinion, pretty successful. Now, the one thing I wish we could see more is the Russian folkore influence, since it's a pretty big theme in Ravka's architecture.

![](https://pbs.twimg.com/media/EzVjafTUUAIETZ8.jpg)

Overall, I was very happy with this adaptation and cannot wait for Season 2!! Big thank you to Macmillan and Netlfix for the new copies of the Grisha Universe book series!

{{% /content %}}

{{% float %}}

### Shadow And Bone

[Trailer](https://www.youtube.com/watch?v=b1WHQTbJ7vE) 

Rating: 5/5

{{% /float %}}
