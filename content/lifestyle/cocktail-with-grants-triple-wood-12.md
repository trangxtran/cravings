---
categories: []
date: 2021-12-21T00:00:00-05:00
title: Cocktail with Grants Triple Wood 12
preview: "media/grants_triplewood.JPG"
---
{{% content %}}

In the past two years of the COVID-19, one good thing came out of it is that I got more time now to have fun with my spirits. Especially, going into the unknown. **I'm more of a gin girl but in the recent months, my interest for whisky grew more and more.** While Gin is widely known in Quebec due to our big reserve of botanicals, its affordability and its popularity, whisky is a lesser known world and also due to the timing. In Canada, whiskys have to be aged for three years in oak barrels in order to deserve its name.

![](media/grants_whiskey.JPG)

This is why most whiskys I've tried so far come from outside of Quebec. Although to be frank, I had a hard time with some Scotch lately due to its smokiness. But as with anything, the more you taste different spirits, the more your palate is refined and starts to notice the _nuance_. One has stuck with me for a while and that has been a complete gem to discover: **Grants Triple Wood 12.**

![](media/grants_triple_wood_12.JPG)

First of all, how amazing does this bottle look with the elegant black and gold design? As for the smell, when poured neat in a Glencairn glass, the first aroma that comes to mind is dried fruits and malty. To the taste, it's a round and balanced flavours and fairly similar to the nose with a fruity sweet but not light and quite delivers in terms of ripe berries. With a few drops of water, its releasing more bread and biscuit flavour and a hint of orange.

What I just described could be summarized to why this is probably my favourite balanced blend. Having matured in 3 different barrels: the American Oak, Bourbon and sherry, this gives the opportunity for the whiskey to be well-round that will please most people.

![](media/grants_triplewood.JPG)

However, everyone knows how much I love mixing cocktails and I couldn't help but create one with Grant's Tripe Wood 12. In a collaboration project with Marie-Eve and Carrie , we each decided to do our take with this beautiful Scotch (be sure to click on the links to see theirs!).

Mine is called _Mon William,_ a dedication to my fiancé who is a Scotch lover but also has the same name as William Grant & Sons.

_Part of a collaboration_

{{% /content %}}

{{% float %}}

_Mon William_ Recipe:

* 3/4 oz Grant's Tripe Wood 12
* 3/4 oz Brandy
* 3/4 oz Yellow Chartreuse
* 3/4 oz Sweet Vermouth
* Dashes of Saffron Bitters (you can use orange bitters or any of your favourite bitters!).
* Stir everything in a mixing glass and strain in an Old Fashioned glass.

{{% /float %}}
