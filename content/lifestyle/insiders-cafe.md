---
categories:
- Coffee Shop
date: 2021-10-17T00:00:00-04:00
title: Insiders Cafe
preview: "media/img_3215.JPG"
---
{{% content %}}

If you're looking for a co-working space in Downtown, Montreal, now is your chance ! I got a chance to discover this new space the other day and totally fell in love with it. The new Downtown location is super spacious, the whole floor has a couch lounge area, with meeting rooms, and board games for your coffee breaks.

![](media/img_3215.JPG)

To start, it's  pretty simple, you create an account on their website : [https://clubinsiders.app/](https://clubinsiders.app/  "https://clubinsiders.app/") and from there, you can reserve a spot, decide what kind of packages do you want. You can use my promo code for **trang20** for 20%off! **They also have a monthly pass, giving you 24-hour access to the space or go by the hour (5$/h) and 17$ the whole day.**

The staff are friendly and they even offered breakfast sandwiches, lunch snacks and a wide variety of lattes and coffees.

![](media/img_8082.JPG)

![](media/img_8077.JPG)

![](media/img_3223.JPG)

{{% /content %}}

{{% float %}}

Insiders Cafe : [App]( "https://clubinsiders.app/") / [Gallery]( "https://www.coworker.com/canada/montreal/club-insiders") 

Rating : 5/5

{{% /float %}}
