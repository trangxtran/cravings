---
categories:
- wine
- lifestyle
- travel
date: 2022-10-16T00:00:00-04:00
title: 'PORTUGAL : WINE PRESS TRIP WITH VINHO VERDE DAY 1 & DAY 2'
preview: "media/quinta-das-arcas-6.JPG"
---
{{% content %}}

This is the 4th time I get to travel for a media trip but it's my first Wine press trip ever ! I was super excited when Sopexa Agency who represents **Vinho Verde** in Montreal reached out and invited me to participate in this trip.

###### The following post will be about my Day 1 and Day 2 itinerary, another post will be published with Day 3 & 4. So stay tuned!

**DAY 1**

Arrived at the Porto airport, we got a driver who came to pick us up to the hotel. We met a few other guests on the media trip and got to make their acquaintance. After checking in at the Sheraton Hotel, we head out for a little tour of the Vinho Verde headquarters in Porto **- Casa do Vinho Verde**. The mansion called Palacete Silva Monteiro. We had a little presentation of the Vinho Verde region. Most Vinho Verde are a blend of native Portuguese white grapes (there is also the red and rosé but less common). Although it means Green Wine, the Vinho Verde appellation represents a region. The rainfall makes the region verdant year-round.

![](media/untitled-design-3.png)

We got our first wine tasting from 3 local producers: Mario Lopes, Adega Ponta de Lima and Terras de Felgueiras. **My favourite of the 3, is probably the** [**Marcio Lopes Vinho Verde**](https://mlw.pt/) **wines overall.** They had some orange and grapefuit notes, fruity but not too sweet.

![](media/day1_manson.JPG)

![](media/day1_vinhoverde.JPG)

**DAY 2**

We checked out of our hotel and headed to our first winery visit.

**Quinta das Arcas**

A very quaint, family business founded by Esteves Monteiro in the early 1980s. My favourites are the ARCA NOVA LOUREIRO and ARCA NOVA ALVARINHO. When one is much more floral to my taste, the other is much more balanced with a ripe fruit note. And for my fellow Quebecers, I was happy to hear that SAQ has the [Arca Nova Alvarinho](https://www.saq.com/fr/14375348?q=arca+nova) (21.70$) ! (if it's out of stock, then it will be restock soon).

![](media/quinta-das-arcas.JPG)![](media/quinta-das-arcas-2.JPG)![](media/quinta-das-arcas-3.JPG)![](media/quinta-das-arcas-6.JPG)

**Quinta de Soalheiro**

A very modern feel to the winery, Soalheiro welcomes us with a wine tasting paired with a lunch feast. When you visit the winery, you will get the feel of a house in a tiny village. It's very cozy and welcoming. They also have a herbal tea garden, a greenhouse growing some lemon basil, peppermint, lemongrass and so on. They are situated in the region of MONÇÃO AND MELGAÇO, the birthplace of Alvarihno grapes.

For the Quebec residents, their **ALLO 2021** (14.70$) is [available in our SAQ.](https://www.saq.com/fr/13553077?q=soalheiro) I got the chance to try it and it's lovely! The blend of Alvarinho and Loureiro confer the green apples and citrusy notes while having a mineral finish. A well-balanced, refreshing wine.

![](media/soalheiro1.JPG)![](media/soalheiro2.JPG)![](media/soalheiro3.JPG)![](media/soalheiro-4.JPG)![](media/soalheiro-5.JPG)

We finished the day with the Monverde hotel winery experience, however that place will be on my next post as the experience is on a 2-day period !

Cheers!

_*this post is in collaboration with the Viticulture Commission of the Vinho Verde Region_

{{% /content %}}

{{% float %}}

In this post:

DAY 1:

* Vinho Verde Introduction

DAY 2: Visit of the wineries

* Quinta da Arcas
* Quinta de Soalheiro

{{% /float %}}
