---
categories:
- classical music
- concert
date: 2022-11-20T00:00:00-05:00
title: 'FESTIVAL BACH: ST JOHN PASSION'
preview: "media/img_2824.JPG"
---
{{% content %}}

I've had the immense privilege to attend the Opening Concert of Festival Bach, St John Passion. The festival is [**_North America’s premiere destination for the timeless musical genius of Johann Sebastian Bach and the cultural legacy that he has inspired._**](https://festivalbachmontreal.com/en/) During the months of November and December, you'll have the chance to listen to Bach's most memorable pieces in the presence of talented artists across Canada and abroad.

The reenvisioned Passion from Hans-Christoph Rademann, the conductor and The Gaechinger Cantorey from the the Internationale Bachakademie Stuttgart form an harmonious and dare I say, glorious music ensemble.

![](media/img_2795.JPG)

For those who are not familiar with this marvelous piece, it narrates the biblical passage, John 18 and 19. Jesus was captured, led before Kaiphas and Pontius Pilate, judged, crucified and put to death. To be honest, the whole piece was sung in German so if you don't speak German like myself, it's hard to follow through. But what helps, is that music is to be felt. And so, I closed my eyes and I could see the scenes happening in my head.

![](media/img_2824.JPG)

One of my favourite parts, is the chorale in the middle of the scene of betrayal and injustice. The moment you realized Jesus's fate is sealed, in that hair-rising moment. This piece is a 1h50 long but so worth it, the complexity and the dynamic of the music makes the whole experience quite unique.

The Bach Festival continues with a beautiful program in the next few weeks:

[https://festivalbachmontreal.com/en/program/](https://festivalbachmontreal.com/en/program/ "https://festivalbachmontreal.com/en/program/")

{{% /content %}}

{{% float %}}

**Festival Bach Montréal:** 

[https://festivalbachmontreal.com/en/](https://festivalbachmontreal.com/en/ "https://festivalbachmontreal.com/en/")

**November-December 2022**

{{% /float %}}
