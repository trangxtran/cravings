---
title: "Bord'Elle"
date: 2023-12-21T12:00:00-05:00
preview: "chandelier.jpg"
---

{{% content width="7/12" %}}

Bord'Elle is a hidden gem that offers a luxurious and unforgettable night out. From the moment you step through the entrance, you are transported to a world of opulence, charm, and vintage glamour.

### Ambiance

Its ambiance is nothing short of captivating. The interior design exudes old-world charm with a modern twist. Plush velvet couches, crystal chandeliers, and rich mahogany accents create an atmosphere of timeless elegance. The dim lighting adds an air of mystery, making it the perfect place for a romantic date night or a sophisticated gathering with friends.

### Live Entertainment
What truly sets Bord'Elle apart is its world-class live entertainment. The performers here are incredibly talented, and the nightly shows are a feast for the senses. From sultry burlesque acts to mesmerizing aerial performances, the entertainment at Bord'Elle is a visual and auditory delight that keeps you engaged throughout the evening.

### Menu
While Bord'Elle is primarily known for its cocktails and entertainment, the food menu is equally impressive. The culinary offerings are a fusion of modern and classic French cuisine, featuring a selection of delectable small plates and entrees. The attention to detail in the presentation and the quality of ingredients make each dish a gastronomic delight.

### Service
The service at Bord'Elle is impeccable. The staff is attentive, knowledgeable, and eager to make your experience memorable. They are well-versed in the menu and can offer excellent recommendations, ensuring that every aspect of your visit is top-notch.

### Dress Code
Bord'Elle has a strict dress code, which adds to the overall ambiance. Guests are encouraged to dress elegantly and in line with the upscale atmosphere of the venue.
In conclusion, Bord'Elle is a place that combines elegance, entertainment to create a truly unforgettable experience. Whether you're celebrating a special occasion or simply looking for a memorable night out, Bord'Elle is the place to be. From the lavish decor to the world-class entertainment and delectable cuisine, it's a destination that deserves a spot on your must-visit list in Montreal.

## New Year's Eve: THE GREAT GATSBY

Bord'Elle's [NYE celebration][] promises to be an unforgettable night with the theme of The Great Gatsby! 

- Live Entertainment: Dance the night away to the beats of live music and talented DJs who will keep the energy soaring all night long.
- Midnight Countdown: Be part of the exhilarating countdown!
- Exclusive Dining Packages: Bord'Elle offers exclusive dining packages for those looking to savor a special dinner before the festivities commence. These packages include entry to the NYE event.
- Dress to Impress: Prepare to dazzle in your most glamorous attire, as Bord’Elle encourages guests to dress elegantly and embrace the extravagance of this enchanting evening.

{{% /content %}}

{{% float %}}

### Details & Info

[*Bord'Elle Boutique Bar & Eatery*](http://www.bordelle.ca/)

Address: [390 Rue Saint-Jacques, Montréal, QC H2Y 1S1][address]

### NYE Event

Tickets sold here: https://bordelle.tixi.ca/

{{% /float %}}

[address]: https://maps.app.goo.gl/tMGMXZwH7i1BTPE28
[NYE celebration]: https://thefarsides.tixi.ca/
