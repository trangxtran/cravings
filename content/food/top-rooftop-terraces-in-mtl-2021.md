---
categories: []
date: 2021-06-30T00:00:00-04:00
title: Top Rooftop Terraces in MTL 2021
preview: "media/img_0503.JPG"
---
{{% content %}}
This is probably the most requested post of all times and the most frequently asked questions: **Trang, where are the best rooftop terraces in Montreal?** Since I live in the Old-Port and had the chance to be on the rooftop terraces since their reopening, I want to jump on the occasion to share a few favourite of mine.

Before I start listing them, in no way this reflects the ultimate list, but I can only speak for the ones that I've been to and I loved. Also, to keep in mind - some have a nice view but not necessarily the best food. However, the ones I mention here are all worth a visit whether you want to enjoy a typical day in the Old-Port or you want a hidden gem terrace.

## [Terrasse Nelligan][]

Situated on the rooftop of Hotel Nelligan, it offers one a few unique atmospheres of Old-Port with traditional stairs and a big space. A very popular destination, reservations strongly recommended.

_Good for: Cocktails and a little happy hour_

![](media/terrassenelligan-1.png)

## [Terrasse Place D'Armes][]

**One of my favourite brunch on a rooftop in the Old-Port !** You should definitely go for their Mimosa Kit, and the delicious pastries platter. Adding to it, a beautiful view to Basilique Notre-Dame.

_Good for: Brunch and Bottomless Mimosa_

![](media/terrasseplaced-armes-copie.png)

## [Perché][]

Probably the cutest rooftop terrace in the Old-Port and nonetheless, with the best view is Terrasse Perché with the entrance door in a little alley. The perfect spot to catch up with friends on a sunny day while enjoying the Place Jacques Cartier magnificent view.

![](media/perche.png)

_Good for: Best view and catching up with friends_

## [Foodlab Lab Culinaire][]

**The best food a rooftop terrace has to offer is on the 3rd Floor of SAT!** I got invited to the media event and it has quickly become my favourite spot with delicious little tapas and the beautiful cocktails !

![](media/foodlab.png)

_Good for: Food tapas-style and cocktails (take the Soupe d'Ortie if you like mushrooms, it's mindblowing!)_

***Up next : Au Jardin Chez Muffy (Quebec City)***
{{% /content %}}

{{% float %}}
**Rooftop Terraces**:

1. [Terrasse Nelligan][]
2. [Terrasse Place D'Armes][]
3. [Perché][]
4. [Foodlab Lab Culinaire][]

Feel free to suggest other MTL Rooftop Terraces to me on @[trangreeny](http://instagram.com/trangreeny)!
{{% /float %}}

[Terrasse Nelligan]: https://terrassenelligan.com/en/home/
[Terrasse Place D'Armes]: https://terrasseplacedarmes.com/en/oasis-urbain/
[Perché]: https://perchemtl.com/
[Foodlab Lab Culinaire]: https://sat.qc.ca/en/restaurant-foodlab-labo-culinaire
