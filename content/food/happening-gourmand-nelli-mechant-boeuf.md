---
categories:
- Dinner
- Restaurant
date: 2023-03-13T00:00:00-04:00
title: 'HAPPENING GOURMAND: NELLI & MECHANT BOEUF'
preview: "media/img_5329.JPG"
---
{{% content %}}

If you're a foodie in Montreal, you've probably heard of Happening Gourmand - an annual event that takes place in March this year. It's a great opportunity to discover some of the city's best restaurants at a discounted price. One of the standout participants this year was Nelli, a cozy Italian restaurant located in the heart of Old Montreal.

![](media/img_5326.JPG)

Nelli is tucked away on a quiet street, but once you step inside, you're transported to Italy. The decor is warm and inviting, with exposed brick walls, wood paneling, and cozy lighting. The menu features classic Italian dishes with a modern twist, and everything is made using locally-sourced ingredients.

![](media/img_5328.JPG)![](media/img_5329.JPG)

During Happening Gourmand, Nelli offers a three-course menu for $42, which is an incredible deal for the quantity of food you get. I started with the tartare and galotyri, which is a greek cheese, soft and spreadable.

For my main course, I chose the bolognese spaghetti, which is one of Nelli's signature dishes you can also ask for the extra lobster. And a classic white wine mussles and fries.

![](media/img_5331.JPG)

Overall, my experience at Nelli during Happening Gourmand was exceptional. The service was attentive and friendly, and the food was absolutely delicious. Even at the regular price point, Nelli is definitely worth a visit if you're looking for a nice wine bar in a cozy atmosphere. It's my second home and I will always vouch for it haha!

![](media/img_4727.JPG)![](media/img_5330.JPG)

_Thank you to the staff for having me!_

{{% /content %}}

{{% float %}}

**Nelli:** [Website](https://nellibistro.com/en/)

**Méchant Boeuf:** [Website](https://mechantboeuf.com/)

{{% /float %}}
