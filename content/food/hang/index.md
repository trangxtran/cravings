---
title: "Hang Bar: New Lunch Experience"
date: 2023-12-19T12:00:00-05:00
preview: "decor.jpg"
---

{{% content width="7/12" %}}

My recent lunch experience at this modern and chic Vietnamese restaurant was nothing short of delightful. From the moment I stepped through the sleek glass doors, I was enveloped in an atmosphere that effortlessly blended tradition with contemporary style.

The restaurant's interior design was a testament to its elegance. Clean lines, dark green and gold colors, and tasteful lighting created a calming and sophisticated ambiance. The combination of dark wood accents and soft textiles added warmth to the otherwise modern setting. Whether you're on a casual lunch date or a business meeting, the atmosphere strikes the perfect balance.

The lunch menu now offers delicious Vietnamese Bowls with vermicelli and a meats of your choice (chicken or beef lemongrass served with fish sauce , tamarind marinated salmon served with fish sauce, crispy tofu served with citrus basil sauce), my highlight was most definitely the BO NE" STEAK & EGGS".  I like my lunch, quick and efficient with all the proteins on the same dish. And of course, the Vietnamese soup Pho is also available at lunch.

## New Year's Eve Extravaganza

Hang’s [NYE celebration][] guarantees an unforgettable night filled with entertainment, exquisite cuisine, and a lively atmosphere. Here's a sneak peek of what awaits you:

- Live Entertainment: Dance the night away to the beats of live music and talented DJs who will keep the energy soaring all night long.
- Midnight Countdown: Be part of the exhilarating countdown!
- Exclusive Dining Packages: Hang offers exclusive dining packages for those looking to savor a special dinner before the festivities commence. These packages include entry to the NYE event.
- Dress to Impress: Prepare to dazzle in your most glamorous attire, as Hang encourages guests to dress elegantly and embrace the extravagance of this enchanting evening.

!["Bo Ne" Steak & Eggs](rice.jpg "Bo Ne Steak & Eggs")

![Pho Soup](pho.jpg "Pho Soup")

{{% /content %}}

{{% float %}}

### Details & Info

[*Hang Bar*](http://www.hangbar.ca/)

Address: [686 Notre-Dame St W, Montreal, Quebec H3C 1J2][address]

**Lunch Menu now available!**

### NYE Event

Tickets sold here: https://hangbar.tixi.ca/

{{% /float %}}

[address]: https://maps.app.goo.gl/PUbhvNcGLrQnsssM9
[NYE celebration]: https://hangbar.tixi.ca/
