---
title: "Farsides: Life of the Party"
date: 2023-12-20T12:00:00-05:00
preview: "noodles.jpg"
---

{{% content width="7/12" %}}

Nestled in the heart of charming Old Montreal, just a stone's throw from the bustling Financial District, you'll discover a culinary gem known as The Farsides. Founded by the visionary John Edward Gumbley and the creative minds at Jegantic, this restaurant is a delightful blend of two distinct cuisines – Thai and Hawaiian – affectionately dubbed "Thaiwaiian."

And this year, 2023, they are celebrating their 4th anniversary with amazing food and menu! his party promises an unforgettable night filled with creativity and passion, just like our unique menu. Adding to it, get ready to groove on NYE  to the beats of our fantastic DJ, the one and only DJ Franzen, celebrated for his work with music legends like Drake, Justin Bieber, and Jay Z.

## New Year's Eve Extravaganza

Hang’s [NYE celebration][] guarantees an unforgettable night filled with entertainment, exquisite cuisine, and a lively atmosphere. Here's a sneak peek of what awaits you:

- Live Entertainment: Dance the night away to the beats of live music and talented DJs who will keep the energy soaring all night long.
- Midnight Countdown: Be part of the exhilarating countdown as we welcome the New Year with a champagne toast and a mesmerizing fireworks display.
- Exclusive Dining Packages: The Farsides dining packages for those looking to savor a special dinner before the festivities commence. These packages include entry to the NYE event.
- Dress to Impress: Prepare to dazzle in your most glamorous attire, as Farsides encourages guests to dress elegantly and embrace the extravagance of this enchanting evening.

{{% /content %}}

{{% float %}}

### Details & Info

[*The Farsides*](http://www.thefarsides.com/)

Address: [690 Notre-Dame St W, Montreal, Quebec H3C 0S5][address]

### NYE Event

Tickets sold here: https://thefarsides.tixi.ca/

{{% /float %}}

[address]: https://maps.app.goo.gl/udAeQ9YhNRwf5pY37
[NYE celebration]: https://thefarsides.tixi.ca/
