---
categories:
- Dinner
date: 2021-09-20T00:00:00-04:00
title: Restaurants I Still Swear By
preview: "media/img_7055.jpg"
---
{{% content %}}

After a summer full of restaurants and terraces reopening (73 spots to be exact), I got to get back and enjoy a great meal with the lovely staff! I'm compiling a list here of my **favourite restaurants that have passed the vibe for me** this summer and hopefully that would help your future cravings in Montreal.

What does it mean to pass the vibe in my world?

- At least one dish is memorable enough to come back a second time
- That I don't leave empty stomach and portions are good enough (I have a big appetite and tend to favor the ones that offer a good amount of food)
- A great customer service experience
- Not sponsored nor part of a collaboration 

## 1) Cadet

This little gem right here was my favourite restaurant since 2019 and I organized my birthday party there and still in 2021, it's on my top list. The menu is tapas mode, where you take a few dishes to share between 2 or 4. My must are always the foccaccia, the fingerling potatoes (believe me, it's divine), their specials of the days are always a good idea.

## 2) Le Serpent

This is a refined restaurant with delicious sashimis, and an unforgettable pasta : Linguini, speck, amandes, chou-fleur, truffe hachée. On their menus, also have the perfect bucatini.

![](media/img_0835.JPG)

## 3) Maison Publique

I would always come back to this restaurant for the Oven Baked Oysters and their most delicious menu. If you want to feast like a king, this is where you should head off to. Amazing pastas, specials of the day and always cook with the freshest ingredients of the season.

![](media/img_0369.JPG)

## 4) Gabrielle Cafe

I've been loving the menu from Gabrielle Café for so long and honestly they never disappoint. From the crudo to the pasta, they know how to bring out flavours and pairing with good wine. Highly recommend!

![](media/img_3996.JPG)

## 5) Marusan at TimeOutMarket

I go back every single time to Timeout Market to get their Black Garlic Ramen and Onsen Tamago. And you should do it too. 

![](media/img_1101.JPG)

## 6) Bar Otto & Bistro Otto

A classic recommendation that I always say to my close ones and friends. If you want some umami flavours and are a fan of high quality Japanese food, these restaurants are for you. The Bone Marrow and Uni Mazemen is the ultimate craving I would always come back for.

![](media/img_7055.jpg)

{{% /content %}}

{{% float %}}
Restaurants:

1. [Cadet](https://www.restaurantcadet.com/en/home)
2. [Le Serpent](https://www.leserpent.ca/)
3. [Maison Publique](https://www.maisonpublique.com/)
4. [Gabrielle Cafe](https://www.instagram.com/gabriellecafe/?hl=en)
5. [Marusan at TimeOutMarket](https://www.timeoutmarket.com/montreal/en/eat_and_drink/marusan/)
6. [Bar Otto & Bistro Otto](https://www.instagram.com/otto.bistro/?hl=en)

{{% /float %}}
