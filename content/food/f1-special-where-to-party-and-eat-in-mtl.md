---
categories:
- happy hour
- dinner
date: 2022-06-08T00:00:00-04:00
title: 'F1 SPECIAL: WHERE TO PARTY AND EAT IN MTL'
preview: "media/yoko-luna_2.JPG"
---
{{% content %}}

With Grand Prix coming up in Montreal, a lot of people have asked where are some spots they can have a good time, dance, party but still eat good food. And so, I came up with this list! It's been 2 years our city hasn't hosted Formula 1 event and it's one of the biggest events to help boost our tourism.

## Restaurant Sauvage (Old-Port)

Best food menu on this list is all thanks to Chef Sary Hussein's work. He transforms this beautiful party place onto a magnificent menu worth a detour. From delicious fresh pasta to a tomahawk steak, Sauvage will have every dish for every palate. Plus, their cocktails are amazing!

![](media/sauvage_1.JPG)
![](media/sauvage_2.JPG)
![](media/sauvage_3.JPG)

## Yoko Luna (Downtown)

A new supper club in the Montreal scene, Yoko Luna offers the most original menu and featuring Nikkei cuisine : blending Japanese and Peruvian flavours. If you're in Montreal for a short trip and want to make your night memorable and treat yourself (or impress your friends), definitely recommending this one!

![](media/yoko-luna_1.JPG)

![](media/yoko-luna_2.JPG)

## Soubois (Downtown)

The newly renovated supper-club has launched with a solid menu from seafood to tomahawk steak and a cocktail card that is as wild as its decor. You are greeted by this apothecary atmosphere with its cabinet of curiosities and trees. At night, the whole tables and restaurant atmosphere are transformed into a night club. ![](media/soubois_1.JPG)

![](media/soubois_2.JPG)

![](media/soubois_3.JPG)

## 212 (Old-Port)

A little hidden from everything else, **212** restaurant offers a fine-dining menu and if you stay late enough, you'll have the chance to get up and dace to the Dj's music. I have a weakness for their terrace, intimate in the Old-Port and yet modern.

![](media/212_night.JPG)

![](media/212.JPG)

All pictures are mine (c) 

Trang T. 

{{% /content %}}

{{% float %}}

In this post:

1. [Restaurant Sauvage](https://www.restaurantsauvage.com/)
2. [Yoko Luna](https://yokoluna.com/)
3. [Soubois](https://soubois.ca/en/)
4. [212 ](https://212montreal.com/)

{{% /float %}}
