---
categories:
- Late Night Snacks
- Happy Hour
date: 2020-09-22T00:00:00-04:00
title: 'SuWu'
preview: "media/IMG_8173.JPG"
---
{{% content %}}
If you're looking for a nice vibe with music you can nod your head to and have a good time with your friends, I strongly recommend Suwu. I had the chance to try out the new late night menu at Suwu and I was gladly  surprised. We start with the new drinks, flavor profile wise, if you like a little floral and sweet, Baba's Juice is the one I would highly recommend for its rose petal and lavender syrup.

![](media/img_8173.JPG)

How the menu works is your order a few dishes that are made to be shared . We got the burrata to start with and it was absolutely delicious. The combination with pesto and burrata with roasted peppers make it for a wonderful fresh feeling.

![](media/img_8176.JPG)

There are still a few classics on the menu that are high in my favourites for example the Mac'n Cheese Grilled Cheese. **That one is an absolute must. The crunchiness of the grilled sandwich with the smooth mac'n cheese and the tomato jam texture just go perfectly together.**

![](media/img_8192.JPG)

Another classic is also their fried chicken with spicy butter sauce, it's not to spicy so give it a try! One of the best fried chicken bites I had in a while.

![](media/img_8184.JPG)

Beef tartare bruschettas were a nice twist on the traditional beef tartare, and for that ***Chapeau!***

![](media/img_8188.JPG)

I have eaten at least 4 times here and every single time has been an amazing experience. Never cease to impress me by the casual. cozy vibe with delicious night snacks to satisfy your cravings.

![](media/img_3665.JPG)
{{% /content %}}

{{% float %}}

### Suwu

[Website](http://www.suwumontreal.com/) --- [Address](https://goo.gl/maps/j5dxSAfW3Af5sLmCA)

**Rating:** 5/5

{{% /float %}}
