---
categories:
- Wine Bar
- Coffee Shop
- Happy Hour
date: 2020-08-17T04:00:00.000+00:00
position: []
title: Cafe Nelli
preview: "media/cafenelli_7406.JPG"
---
{{% content %}}
Introducing you to our lovely addition in my neighborhood, in the Old-Port: ***Cafe Nelli.***

![Large street-facing opened windows](media/cafenelli_7386.JPG)

![Interior decor with plants and bas-relief](media/cafenelli_7387.JPG)

[**Cafe Nelli**](https://nellicafevin.com/en/) is a new project from Hotel Nelligan, they have successfully created a place that answers **my ultimate needs: a coffee shop during the day so I can work on my laptop and a wine bar at night to taste all the organic wines** they have to offer accompanied with a beautiful menu: tartare, oysters, specials of the night, fried octopus, we got a platter of their roasted chicken and it was a well-worth menu.

![Pastries and iced chocolate](media/cafenelli_7394.JPG)

They also have a special Happening Gourmand menu for 25$, for people with a bigger appetite like me, I recommend getting the roasted chicken option, you'll thank me later :)

![Oysters and charcuterie platter](media/cafenelli_7437.JPG)
![Posing by the street view window](media/cafenelli_4293.JPG)
![Cheers to red wine and roasted chicken](media/cafenelli_7490.JPG)
![Orange wine and coffee table](media/cafenelli_7434.JPG)

A little warning:  Once I fall in love with a place, I tend to go back to the same place multiple times and recently, I've been to  Cafe Nelli + Bar À Vin  at least twice a week for a few weeks now and each time, the staff was welcoming and happy to see us. Their couch besides the window, may or may not be our official spot now ;)

{{% /content %}}
{{% float %}}
### Cafe Nelli

[Website](https://nellicafevin.com/en/) --- [Address](https://goo.gl/maps/krpjaN5VefNm2EAv5)

**Rating:** 5/5

Happening Gourmand at 25$

*Tip: Ask for the roasted chicken ;) The platter is worth it!*
{{% /float %}}
