---
categories:
- Old Montreal
- Bar
- Dinner
date: 2020-06-24T04:00:00.000+00:00
position: []
title: Parliament
preview: "media/img_4350.JPG"
---
{{% content %}}
I often wonder where the chefs and bartenders find their ideas. How they come up with the final product and what kind of elements and thoughts go into their minds to sculpt everything.

![The Trang cocktail](media/20191023105245_img_4719.JPG)

***Parliament*** has been on the top of my favourites of 2019 and for a good reason. Not only they are one of the classiest pubs in Montreal, they offer a drink menu both original and savoury combined with the talented food menu created by the one and only Chef Chanthy.

![The making of the Trang cocktail](media/20191129112938_img_5932.JPG)

The first day of our Parliament discovery, we were walking around Old Port after a good day's work. Me and William have always wondered what kind of bars and drinks our neighborhood would offer and decided to go bar hopping. Parliament was situated in this cute corner between Place D'Youville and Rue Saint Pierre that caught our eyes immediately. We would come sit by the bar and was welcomed by our first bartender of the night, Harry, who would quickly become William's favourite bar buddy.

Refined, approachable, passionate and attentive, he grasped our flavor profiles right away after a few drinks and has created a customized drink for each of us. The ***Trang cocktail*** is still my favourite drink in all of Montreal for it combines sweet, efflorescent and quite the floral tribute.

As a food enthusiast, I couldn't help but try the menu that Parliament offered. I have to admit, I was skeptical at first because the food offered  in bars don't often meet my expectations. But their menus were very satistfying. And I came back every week after that to try Chanthy's specials of the day.

With the restaurants slowly reopening after confinement, the team of Parliament offers a lovely terrace and a new popup calls [TOUK](https://www.toukmtl.com/ "TOUK"), which allows Montrealers to be transported to Cambodia and try their delicious national dishes and I'm all here for it.

![The Frosé cocktail](media/img_4350.JPG)
![Soup noodle of the week](media/img_4379.JPG)
![Strawberry negroni](media/img_4360.JPG)
![The Khey F Cee](media/img_4369.JPG)

Looking forward to their weekly specials !
{{% /content %}}
{{% float %}}

### Parliament Pub & Parlour

[Website](https://www.parliamentmtl.com/) --- [Address](https://g.page/parliament-pub-parlour)

**Rating:** all the stars in the world

*Check out their Cambodian menu [Touk](https://www.toukmtl.com/)*
 {{% /float %}}
