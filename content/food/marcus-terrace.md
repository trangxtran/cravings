---
categories: []
date: 2021-03-16T00:00:00-04:00
title: 'Marcus'
preview: "media/img_0363.JPG"
---
{{% content %}}

After months of quarantining, I was very excited to be back on the terrace of Marcus. Impeccable design and service as always (however the waiters didn't really explain to us what the dishes were, or what makes them special, where they are from)  but what I was looking forward to was to eat the dishes on a table, served fresh. We started with the oysters: great portion, fresh and juicy.

![](media/img_0362.JPG)

I really liked my experience before the pandemic and I craved for their classic lobster, crab pasta. To my surprise, I was quite disappointed by this year's version. **The spaghetti picadilly was overshadowed by the chili, I honestly didn't taste the crab nor the uni butter.** I've eaten in most of the fine dining places in Montreal and while I don't mind spending that much on a dish, I expect the dish to be flavourful and deliver on its quality.

![](media/img_0363.JPG)

Up next, I got the Black Cod  with Sesame, Miso, Potato, Lime, Edamame, this one was a pure heaven to the mouth but once again for a 38$ dish, I expect more than a 6x12 cm piece of fish. It would definitely go in my category of appetizer due to the size of it.

![](media/img_0351.JPG)

For cocktails, I got **NAZCA Y CANAIMA** and the **BOHO**. The latter definitely scores in my cocktail favourites, however the Nazca Y Canaima didn't deliver in tastes nor in the alcohol side, it simply tasted like diluted pomegranate juice.

Overall, the experience and food was pretty average however, please keep in mind that it was during their first opening weekend (with new staff, a pandemic on our shoulders) and so I'm sure the experience will get better as they will start to settle in.  

![](media/img_0354.JPG)

_NB. Purchased everything._

{{% /content %}}

{{% float %}}

### Marcus

[Website](https://www.fourseasons.com/montreal/dining/restaurants/marcus-restaurant-and-terrace/ "Marcus")

*All the waiters wore masks and were at the right distance while serving us!*

{{% /float %}}
