---
title: "Yoko Luna"
date: 2023-12-01T17:05:25-05:00
preview: "yoko_luna-statue.jpg"
---

{{% content width="7/12" %}}

[Yoko Luna][] has long been celebrated for its unique fusion of Japanese elegance and contemporary style. As we approach the end of the year, this beloved establishment has a double treat in store for its patrons. First, they're unveiling an all-new [Ladies Night menu][menu] **every Sunday** that promises to delight, with a 49$ menu comprising 3-courses. And second, Yoko Luna is gearing up for a New Year's Eve extravaganza that's set to be the talk of the town.	

## 49$ MENU

For the first-course, you have the choice of:

- FRESH MARKET OYSTERS (4) &mdash; *with house mignonette.*
- GRILLED HEIRLOOM & PINEAPPLE SALAD &mdash; *Heirloom tomatoes, grilled pineapple, tomatillo salsa, shiso, Mint & Mascarpone.*
- TUNA TARTARE &mdash; *Bluefin tuna, ponzu mayo, radish, crispy rice, yuzu.*

For the main: 

- CHEF'S OMAKASE (12) &mdash; *Our finest selection of hand rolled and fresh fish straight from Japan 6 oz.*
- FILET MIGNON &mdash; *6 oz AAA Black Angus filet mignon, yoko mash, asparagus, quebec mushrooms.*
- ARGENTINIAN SEA BASS &mdash; *Grilled seabass, annatto marinade, grilled asparagus, chimichurri.*

![Tuna tartare](yoko_luna-sushi.jpg "Tuna tartare")

## Creative Cocktails

Ladies Night at Yoko Luna wouldn't be complete without their signature cocktails. The mixologists have curated a menu of refreshing drinks that perfectly complement the cuisine. All the cocktails on their list were a delight. 

![Cocktail](yoko_luna-cocktail.jpg)

## New Year's Eve Extravaganza

[Yoko Luna's New Year's Eve celebration][NYE] promises to be an unforgettable night filled with entertainment, delectable food, and a vibrant atmosphere. Here's a glimpse of what you can expect:

- Live Entertainment: Dance the night away to live music and DJs who will keep the energy high throughout the evening.
- Countdown to Midnight: Join the countdown to welcome the New Year with a champagne toast.
- Exclusive Dining Packages: Yoko Luna offers exclusive dining packages for those who want to indulge in a special dinner before the festivities begin. These packages include access to the NYE event.
- Dress to Impress: Don your most glamorous attire as Yoko Luna encourages guests to dress to impress for this night of elegance and extravagance.

![Photo grid](yoko_luna-grid4x4.jpg)

Personally, the Ladies Night Menu is such a good deal, sog gather your friends and make your reservations! Cheers to a fantastic year ahead!

{{% /content %}}

{{% float %}}

### Details & Info

Address: [Yoko Luna 1234 de la Montagne, Montreal, Quebec, H3G 1Z1][address]

**Ladies Night at Yoko Luna**

Every Sunday: [*omakase menu at 49$/person*][menu]

### NYE Event

Tickets sold here: https://yokoluna.tixi.ca/

{{% /float %}}

[address]: https://maps.app.goo.gl/rY7AzEFqEpmZRaFG6
[menu]: https://yokoluna.com/wp-content/uploads/2023/11/LADIES_49_MENU.pdf
[NYE]: https://yokoluna.tixi.ca/
[Yoko Luna]: https://yokoluna.com/
