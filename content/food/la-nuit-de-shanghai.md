---
categories:
- Bar
- Restaurant
date: 2021-12-05T00:00:00-05:00
title: La Nuit de Shanghai
preview: "media/img_4045.JPG"
---
{{% content %}}

There are a few restaurants and bars that have made an impact in my life and I'm happy to say that La Nuit de Shanghai - a gastronomic cabaret, is definitely one of them and even more, it convinces me to visit DIX30 a bit more often.

We were welcomed with percussions on red neon lights _Shanghai Never Sleeps_ shining, making a statement that the place wants us to know that we will be in for a good night. When you enter the dragon's den, it's dark and moody then everything set in motion : _Fly Me To The Moon_ live version sang in a melodic jazz way that brings you back to the 60's, the design is elegant and chic, walking straight forward an impressive view of the bar accompanied by majestic couches and seats.

![](media/img_4119.JPG)

I was gladly impressed by the cocktails menu (kudoos to the barchef!). It's not easy to mix the Asian flavours with the classics cocktail but at La Nuit de Shanghai, they seemed to that effortlessly. Food wise, it's a tapas menu, very efficient and still tasty that pair well with the drinks.

![](media/img_4043.JPG)

While sipping on the cocktails and exchanging with my friends, we got a few surprise performances that have enhanced the whole experience. I am recommending that you go to this place at least once in your life to witness its beauty and enjoy the festivities.

![](media/img_4045.JPG)

![](media/img_4049.JPG)

![](media/img_4050.JPG)

{{% /content %}}

{{% float %}}

### La Nuit de Shanghai

[https://lanuitshanghai.ca/en/](https://lanuitshanghai.ca/en/ "https://lanuitshanghai.ca/en/")

Rating: 4/5

_Media Event Night_

{{% /float %}}
