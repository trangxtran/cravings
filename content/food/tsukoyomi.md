---
title: Mile-End enters the ramen game
date: 2018-07-22T19:59:11.000-05:00
categories:
- Lunch
- Dinner
- Ramen
preview: "media/tsukuyomi-0.jpg"
---
{{% content %}}
I discovered Tsukuyomi at **YATAI Montreal**, an asian food festival, where they served their cold noodle in a cute little bowl for 8$. The cold ramen was delicious and so I wanted to try the real restaurant as well.

When you enter, a go-around wooden counter reminds me of the traditional Japanese Ramen booth feeling. We sit in front of the cooks themselves and so we can see them work. For appetizers, we ordered the famous **Kara-Age (fried chicken) and Takoyaki (octopus balls)**. The two appetizers that every Japanese should have haha.

**The Kara-Age was really good**, a tender meat along with a lemon-salty sauce and the mayonnaise was just enough to accentuate the flavour. What’s really impressive is the flour around the fried chicken, it’s nothing like I’ve seen before. Kara-Age flour is usually thick and crunchy, this one seems lighter and easier to swallow.

**The Takoyaki** was everything I was hoping for, very mushi on the inside and a tender on the outside. Montreal Japanese Restaurant all have this basic Takoyaki recipes that I haven’t found one that is bad or extremely mind-blowing either. They taste pretty much the same for now.

However, the real question is : **HOW IS THE RAMEN?!**
WELL, even though the texture reminds a lot of Yotako Yokabai, and people keep comparing those two, nothing is similar. First, the broth here is a bit more pronounced on the pork flavor and much lighter than Yokato Yokabai. The marinated egg is sweeter and a little bit more overcooked, the yolk is less runny. The chicken is quite ordinary if you don’t like salty terriyaki chicken, this one is a great choice to go along with the ramen because it’s a neutral item. The pork here is a bit dry compared to the fatty pork pieces from Yokato Yokabai.

Overall, it depends on what you like in a bowl of Ramen!
**I would recommend this place for those who like:**

- A lighter broth, not thick nor creamy
- A less fatty meat
- More veggies on their bowl
- Less runny egg yolks

![The bar counter](media/tsukuyomi-1.jpg)
![Their business card](media/tsukuyomi-2.jpg)
![The edamame](media/tsukuyomi-3.jpg)
![The takoyaki](media/tsukuyomi-4.jpg)
![The karaage](media/tsukuyomi-5.jpg)
![Noodle pull](media/tsukuyomi-6.jpg)
![Noodle pull](media/tsukuyomi-7.jpg )
![The chicken and the broth](media/tsukuyomi-8.jpg)
{{% /content %}}

{{% float %}}
# Tsukuyomi

[Website](https://www.tsukuyomi.ca/) --- [Website](https://goo.gl/maps/BARm3QTjVV67uVnq6)

Rating : 4/5 

*Tip: Order the Cold Ramen on a summer night ;)!*
 {{% /float %}}
