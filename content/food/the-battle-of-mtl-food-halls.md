---
title: The Battle of MTL Food Halls
date: 2020-02-10T20:47:02.000+00:00
categories:
- Food Hall
- Supper
- Lunch
- Downtown
- Happy Hour
preview: 'media/uniburger.jpg'
---
{{% content %}}
After posting my pictures of the opening of Le Cathcart, the question I got asked the most was : who's the best between the 3 : **_Le Cathcart_**, **_Time Out Market_** or **_Le Central_** ? Honestly, as much as we love to see a battle to the death and competition, I would say that the three offer a very unique experience that everybody can enjoy. The three food halls have three different vibes that made me think of three different personalities.  **_So the question is: What kind of Food Hall are you?_**

## The Fun & Sweet Friend - Le CENTRAL

The funky atmosphere of Le Central and the affordable options are for those people who love a good fun, while wearing comfortable clothes. Each restaurant has their own design which reflects the multi-options of Le Central.  You will see familiar places and renowned for their comfort food like **_La Cantina Emilia_**,**_Super Qualité_**, **_Misoya Ramen._** They also offer the most sweet options of all 3 food halls with **_Trous de Beigne_**, **_Bagado_**  and **_Mignon Churros and Nougat_**.

## The Classy and Chic Friend - Time Out Market

_Time Out Market_ offers a very unique experience: dim lights, dark atmosphere which all contribute to the particular mood of restaurant where you would go to have supper and candles.  The restaurants are also one of the finest's names in Montreal featuring **_Marusan_**, **_Le Club Chasse et Pêche_**, **_Il Miglio_** , **_Foxy_** and many more. What comes with fine reputation also comes with a higher price. It is more expensive than Le Central but quite affordable for the quality of food you are getting.

![Timeout Market penne pasta](media/timeoutmarket1.jpg)
![Timeout Market ramen](media/timeoutmarket2.jpg)
![Timeout Market ravioli](media/timeoutmarket3.jpg)

## The Aesthetic and Creative Friend - Le Cathcart

Accomodating 1,000 seats and a beautiful Biergarten, full with greenery and a modern industrial design. With a glass ceiling that makes all the ambiance brighter and receiving more sunlight than the other 2 food halls, Le Cathcart is showcasing the Sid Lee Architecure work with a lot of green inspirations.  Foodwise, I think it's perfect to pass by for a quick lunch with my favourites from the grilled cheese at Patzzi, grab a quick hot chocolate at Cafe Veloce and the famous burger from Uniburger. Mirabel Brasserie is one of the highlights of Cathcart with delicious burrata appetizers and beef ragu pasta. You can even bring your own lunch to Le Cathcart and take advantage of the sunlight and tropical side of Montreal.

![Akio sushi bar counter](media/akio-comptoir.jpg "Akio Comptoir")
![Toro sashimi and maki](media/akio.jpg "Akio Sushi")
![Couch and coffee table in the biergarten](media/cathcart.jpg "Biergarten")
![Burger and fries](media/uniburger.jpg "Uniburger")
![Grilled cheese and two sauces](media/patzzi.jpg "Patzzi grilled cheese")
![Biergarten view from top of the stairs](media/biergarten.jpg "Biergarten")
![Coffee cup from Cafe Veloce](media/veloce-cafe.jpg "Coffee")
![Cafe Veloce's menu](media/veloce-comptoir.jpg "Cafe Veloce")

That's the good news about Montreal diverse culinary scene, no matter you are in the mood for, there are plenty of options and places to go to.
{{% /content %}}

{{% float %}}
### Cathcart
[Address](https://goo.gl/maps/VcNcJvc8FK67sj3P6) --- [Website](https://lecathcart.com)
Rating: 3/5

### Timeout
[Address](https://goo.gl/maps/x93rxFxbFbE7vPEQA) --- [Website](https://timeout.com/montreal)
Rating: 4/5

### Central
[Address](https://goo.gl/maps/fgWR281pBFJVdNgi7) --- [Website](https://lecentral.ca)
Rating: 3.5/5
{{% /float %}}
