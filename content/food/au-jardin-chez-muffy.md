---
categories: []
date: 2021-07-14T00:00:00-04:00
title: Au Jardin Chez Muffy
preview: "media/img_3683.JPG"
---
{{% content %}}

[***Au Jardin Chez Muffy***](https://www.saint-antoine.com/fr/dining/au-jardin-chez-muffy) is the new summer spot to go when you're in **Quebec City!** When entering the restaurant, a rustic greenhouse atmosphere sets the tone and the decor right away. The little pearls of lights against the greenhouse spheres and an adorable swing in the middle. Recycling and the mission for environment was at the heart of this project, so all their decorations were their hotel inventory that has been recycled.

![](media/img_3686.JPG)

On the cocktail side, I was impressed by their drinks menus but I couldn't go wrong with the Spritz D'Alex, a floral and sweet profile to start the night.

![](media/img_1238.JPG)

In the kitchen, chefs Alex Bouchard and Arthur Muller have curated a menu that emphasizes the flavours of the hotel garden. We started with their cold soup, melon and chorizo, very refreshing. We also had the scallops, strawberries and edible flowers, scallops were of  high quality and the touch of strawberries were a nice add. The oysters, garlic leeks were a delicate finish to our "appetizers". For the mains, we had the lamb tartare, the halibut, chervil and grilled cucumbers ; and the boar smash burger.  All three items were a joy to taste. The lamb was really smooth, as for the halibut it was tender and the grilled cucumber balances it out with its texture.

![](media/img_1237.JPG)

But my highlight of the night was by far the Boar Smash Burger. The juicy and flavourful meat and the homemade tomato sauce just made my night.

![](media/img_1239.JPG)

Overall, I highly recommend the experience, it's small tapas portions but it is refreshing and full of aromas and deliciousness.

![](media/img_1234.JPG)

![](media/img_3683.JPG)

{{% /content %}}

{{% float %}}

### Au Jardin Chez Muffy

[Website](https://www.saint-antoine.com/fr/dining/au-jardin-chez-muffy "https://www.saint-antoine.com/fr/dining/au-jardin-chez-muffy") --- [Address](https://goo.gl/maps/7NGXVKYKGeuF46SDA)

**Rating : 4/5**

{{% /float %}}
