---
categories:
- Japanese
- Little Burgundy
- Dinner
date: 2020-07-12T04:00:00.000+00:00
title: Bar Otto
preview: "media/barotto3.JPG"
---
{{% content %}}
![Otto Bar preview](media/bar_otto.jpg)

*UPDATE - July 12, 2020 : I had started this review back in end of February and going into this review again after the big confinement and the restaurants closing events feel kind of weird. But, I still stand on promoting the restaurants that I have always adored and loved. Bar Otto was among the ones who have reopened and I was really excited to try it out again and hope this will shine a light on their great experiences!*

There are places in Montreal I find myself going back to, not only because of their food but also because of the team that make the experience. The Otto family falls into this category. I made a small inventory of my restaurant visits and I have been at Otto Bistro at least 6 times in 2019 haha.

![Duck mazemen](media/barotto1.JPG)

**Otto Bar MTL** is located in one of the busiest areas of Montreal, on Notre-Dame. Next to great restaurants like Joe Beef and Bon Vivant and coffee shops like Barley and Lily and Oli. So for those who cannot venture to the plateau for Otto Bistro, they can now do so in the Lionel Groulx metro!

![Uni sashimi](media/barotto3.JPG)
![Drinks](media/barotto4.JPG)

Let's dig into the food! The stars of the show are probably the specials of the day, and I think it's a safe bet when you go to any restaurant and you want to discover a new dish just ask for the specials of the day! For Otto, I had the chance to have the beautiful **pan seared Foie Gras**, which was an absolute delight to eat. **Adding to it, the Temaki Uni and Otoro and Temaki Lobster Otoro** also put the night's expectations really high.

![Bar decor](media/img_5157.JPG)
![Temaki uni](media/img_5175.JPG)
![Pan-seared foie gras](media/img_5174.JPG)

For the main, we went with our most beloved **Uni Mazemen and Bone Marrow**. If you never had mazemen before, it's a no-broth ramen topped with different ingredients and garnishes. Here in Otto, **they add Uni and Bone Marrow which are my two favourite things in the world.** The creamy and smooth sense of Uni combined with the Bone Marrow just enrich the whole meal.

![Uni mazemen and bone marrow](media/img_5196.JPG)

***Social Distancing : Respected in my opinion, I didn't feel uncomfortable, they had plant separators between the tables as well and all the staff was wearing their masks all the time.***

Before I end this review, I just want to give a quick shoutout to all the staff for always being cheerful and make us feel welcome in every of our visits :)


{{% /content %}}
{{% float %}}

### Bar Otto

[Facebook](https://www.facebook.com/barotto.mtl/) --- [Address](https://g.page/bar-otto-montreal)

**Rating:** 5/5
{{% /float %}}
