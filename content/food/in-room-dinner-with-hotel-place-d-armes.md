---
categories:
- Happy Hour
- Japanese 
date: 2021-04-18T00:00:00-04:00
title: "In-Room Dinner with Hôtel Place D'Armes"
preview: "media/img_9326.jpg"
---
{{% content %}}

On Saturday I had the chance to try out the [**new In-Room Dinner Experience**](https://brasserie701.com/en/packages/)offered by **Hôtel Place d'Armes** with **Brasserie 701** and **Kyo Bar** menus in collaboration with [***Experience Old-Montreal.***](https://experienceoldmontreal.com/en/about/)

![](media/img_9326.jpg)

Due to lockdowns, dine-ins are not allowed at the moment in restaurants. And that's where the new experience at Hôtel Place D'armes comes in, **you get a room to yourself to make you feel as if you are living your own restaurant experience with a view to the Old-Port.** COVID measures were respected, all the staff always wore masks and there were no one else in the room.

This service is available **Friday and Saturday** in Hôtel Place D'armes, William Gray Hotel and Hotel Nelligan as well but with different menus.

![](media/img_6383.jpg)

Upon entering, you spot right away the table dinner waiting for you with nice windows that sit upon a dreamy Old-Port view. Menu-wise, they really hit the spot with a Japanese fusion menu from Kyo Bar and Brasserie 701. We got our first two cocktails with some amuse-bouche.

![](media/img_6379.jpg)

![](media/img_6382.jpg)

![](media/img_6380.jpg)

For appetizers, we chose **Ebi Mayo Shrimp** and **Tataki Tasting.** Both were flavourful, the Ebi Mayo Shrimp would always be a favourite of mine for its creamy feeling and rich in textures.

![](media/img_6408.jpg)

For the mains, we went with **Filet Mignon** and the **Platter of Nigiris and Makis tasting**, I definitely recommend the latter. The nigiris were selected by the chefs, one of the rolls we had was a snow crab and lobster roll with a secret sauce garnish.  We chose a bottle of Sake to go along with this dish.

![](media/img_6418.jpg)

![](media/img_6429.jpg)

![](media/img_6431.jpg)

For desserts, **Yuzu donuts, sesame condensed milk** and **Cheesecake 701**. For a sweetooth like me, the donuts were right at my level whether my boyfriend preferred the cheesecake with its lime and meringue touch to it.

![](media/img_6433.jpg)

![](media/img_6435.jpg)

Living in the neighborhood I always try to discover new places and hidden gems. Therefore, to accompany this In-Room Dinner experience, and here are my recommendations on what to do in the Old-Port of Montreal:

- Coffee stroll at [NELLI Cafe](https://trangscravings.com/food/cafe-nelli/)
- Visting the RAILROAD exposition at [Musée Pointe-A-Callieres](https://pacmusee.qc.ca/en/)
- If you're a bookworm, I recommend [Librairie Bertrand](https://librairiebertrand.com/)
- Head to [Marché Bonsecours](https://www.instagram.com/p/CNDoTJXDTYP/?igshid=jobj45ulh0nd) and St-Paul Street for some nice Insta-worthy pictures

![](media/img_6296.jpg)

*A big thank you to Experience Old-Montreal for this invitation!*

{{% /content %}}

{{% float %}}

Dinner Night Out Experience: [Website](https://brasserie701.com/en/packages/)

Hôtel Place D'Armes: [Website](https://hotelplacedarmes.com/)

{{% /float %}}
