---
categories:
- Happy Hour
- Quebec Distillery
- Cocktails
date: 2020-07-23T04:00:00.000+00:00
position: []
title: Spiritueux Iberville
preview: "media/img_2002.JPG"
---
{{% content %}}
A few months ago, Spiritueux Iberville offered me the chance to shoot some of their products and at the same time, coming up with some drinks featuring a few of their best-sellers.

## Miele Amaretto

The first Amaretto in the world to use honey extract which is pretty original. If you're a sweetooth like me, this would totally my go-to amaretto. You can easily drink it on the rock or make a simple cocktail with some tonic water.

![Godfather cocktail](media/img_1700.JPG)

## Amernoir

This digestive is peculiar in its genre : featuring a light taste of spiced coffee and can be drink as a shot or on ice. If you're looking for cocktail, it pairs well with any rum and whiskey.

![Amernoir bottle](media/img_1960.JPG)

## Amermelade

An apperitivo worthy of the name, we can find the smell of Myrica gale and Calendula Extract, to be paired with a nice Aperol Spritz or I go for a little more fancy cocktail with Peach Schnapps, Amermelade, your favourite gin and lemon juice, shaken with a white egg.

![Straining a cocktail with Amermelade](media/img_1717.JPG)

## Pomodoro

Unfortunately, this one is probably one of my least favourite from the Spiritueux Iberville collection simply because I find it hard to be creative with these kind of cocktails. It's a rich tomato-basil infused liquor which is great for bloody ceasar but make it highly difficult for other cocktails. If you know a good recipe featuring this kind of product, please let me know!

![Straining a cocktail with Pomodoro](media/img_1765.JPG)
{{% /content %}}
{{% float %}}
### Spiritueux Iberville

[Website](https://spiritueux-iberville.com/)
{{% /float %}}
