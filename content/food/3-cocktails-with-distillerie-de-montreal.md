---
categories:
- Happy Hour
date: 2021-03-07T00:00:00-05:00
title: 'Cocktails with Distillerie de Montréal '
preview: "media/img_7512.JPG"
---
{{% content %}}
Hi all!

I want to introduce you to one of my favourite distilleries at the moment: [**Distillerie de Montreal**](https://www.distilleriedemontreal.com/). I had the chance to try out their products throughout the year and I got to admit that I found myself re-using them quite often. **I created 3 cocktails for 3 different occasions.**

First off, for brunch :

**The Daybreak**

* 2 oz **Rosemont Rum Épicé**
* 3/4 oz **Orange Electrique**
* 1/2 oz Donn's Mix
* 1/2 oz Fresh Blood Orange Juice
* Shake everything and pour into a low glass

![](media/img_4682.JPG)

For those late night drinks, here's the :

**Slay in Red**

* 2 oz **Rosemont Vodka**
* 3/4 oz Cranberry Juice
* 3/4 oz Pomegranate Syrup
* 1/2 oz Lemon Juice
* Shake everything and pour in a chilled coupe

![](media/img_9841.JPG)

A twist on the *French Pearl* for those apéros vibe:

* 2 oz London Dry Gin
* 3/4 oz lime juice
* 1/2 oz honey syrup
* 1/4 oz **Rosemont Pastis Liquor**
* 4 mint leaves
* Shake everything and pour in a chilled coupe

![](media/img_7900.JPG)

My favourite spirit from them would probably be the **Rosemont Vodka**, a smooth and sweet end to it which makes it one of my favourite vodkas so far. Fun fact: 10 000 L of Quebec maple water were needed in producing the first batch of this vodka!
{{% /content %}}

{{% float %}}
# Distillerie de Montréal

[Website](https://www.distilleriedemontreal.com/) --- [Products](https://www.distilleriedemontreal.com/produits)

**Ratings (neat) :**

1. Rosemont Vodka : 5/5
2. Rosemont Spiced Rum: 4/5
3. Rosemont Pastis: 3.8/5
4. Rosemont White Rum: 3/5
{{% /float %}}
