---
categories:
- Coffee Shop
- Saint-Laurent
date: 2020-03-16T04:00:00.000+00:00
title: 'Cafe Le VSL'
preview: "media/cafevsl4.jpeg"
---
{{% content %}}
I've lived in the neighbourhood of Ville Saint-Laurent for more than 10 years and one of the saddest thing about it is that I wished there were more coffee shops on the block.

When Cafe VSL opened on the Décarie Street, it proves that Ville Saint-Laurent can welcome the independent and local coffee shops. The decor is a simplist woody atmosphere. On top of that, the owner is one of the sweetest people I've ever met.

Now, in terms of drinks and food, you will not be disappointed. I am OBSESSED with Tai's homemade cookies, it melts in your mouth after the first bite. Oh and you don't have to travel all the way to California to get cute colorful drinks ! We have it all here in Montreal.

![Latte selection](media/cafevsl1.jpeg)
![Latte art](media/cafevsl2.jpeg)
{{% /content %}}
{{% float %}}
### Cafe Le VSL

[Website](https://cafe-le-vsl.business.site/) --- [Address](https://g.page/cafelevsl)

**Rating:** 5/5
{{% /float %}}
