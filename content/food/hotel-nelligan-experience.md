---
categories: []
date: 2021-05-18T00:00:00-04:00
title: Hôtel Nelligan Experience
preview: "media/img_9867.JPG"
---
{{% content %}}

Living in the Old-Port allows me to connect with more and more local business in the neighborhood. Last year when NELLI Cafe opens up, I ended up being there almost every day and I had become quite attached to the staff. NELLI Cafe is situated right inside of Hotel Nelligan building and I was curious about the hotel experience. Luckily for me, I had the chance to discover the whole experience for a whole day, from the dinner in-room atmosphere to the lovely breakfast in the morning.

![](media/img_9501.JPG)

## The Staycation

The first thing I notice when I enter the room is how big the room was, the bed was super comfortable and I couldn't help but jump on it as soon as I see it. The little chimney touch also adds a chic accent to the room, but my favourite corner would probably have to be the windows and the view. Very Parisian-like, I open up the window and  I can see the old buildings on St-Paul St pacing side by side.

![](media/img_9526.JPG)
![](media/img_9867.JPG)
![](media/img_9725.JPG)

## The dinner in-room experience

Similar to the concept of my recent post about [Hotel Place D'armes](https://trangscravings.com/food/in-room-dinner-with-hotel-place-d-armes/), Hôtel Nelligan also offers the package of having Restaurant atmosphere in a separate room. Probably one of my favourites so far, the room has a beautiful chimney with plants all around, a cute little corner table on the window side and the main table with a black drape in the center.

![](media/img_9686.JPG)

_You don't have to wear your masks since it's your own separate rooms, no other clients are in the room with you. While moving around the Hotel however, the mask must be put back on._

![](media/img_9636.JPG)

The most impressive part was without a doubt the menu. I had arrived on time for their new menu uncovering featuring grilled octopus, a beautiful plated foie gras torchon, risotto, buttersquash and mushrooms. To finish with a gorgeous chocolate mousse dessert and raspberries goulet.

![](media/img_9743.JPG)
![](media/img_9750.JPG)

## The breakfast experience

The next day after a good night sleep, we tried the breakfast menu from NELLI Cafe, with their Continental and the Classic Breakfast option. A generous portion and we enjoyed it on the window side looking out to St Paul Street.

![](media/img_9838.JPG)

_A big thank you to the team of Hotel Nelligan and Experience Vieux-Montreal for making it a wonderful stay and for this collaboration!_

![](media/img_9737.JPG)

{{% /content %}}

{{% float %}}

In-Room Dinner Verses Bistro : [Website](https://versesrestaurant.com/en/packages/)

NELLI Cafe: [Website](https://nellicafevin.com/en/)

Hôtel Nelligan:[ Website](https://hotelnelligan.com/hotel/)

*All the waiters wore masks and were at the right distance while serving us!*

{{% /float %}}
