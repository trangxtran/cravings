---
categories:
- coffee crawl
date: 2021-04-17T00:00:00-04:00
title: Coffee Crawling in MTL
preview: "media/img_8498.JPG"
---
{{% content %}}

As a big coffee (or should I say hot chocolate) crawler, it was sad not to be able to hang in coffee shops anymore, it was my safe heaven with a book in my hands or simply working on my laptop or catching up with friends. But luckily, we can still visit and support our local businesses. Had the chance to discover a few **new coffee shops and revisit old ones** during the past months and thought it would be a good time to share with you all !

## Café Coquetel

**Probably one of my favourite discoveries of the year!** I was walking by with my friend when we stopped and realized it’s a new coffee shop and the names inspires us right away (big fan of cocktails here). As you enter, it’s a cute little space where you get greeted by a cute cherries neon sign, where you can see a few spirits behind the counter and I knew this was some blend of a coffee shop and a bar. I chatted with the baristas both were very kind and explained to me the drinks they serve. **In normal circumstances, yes, you can get your cocktail while hanging out in this coffee shop but due to the regulations,** they brilliantly transformed their drinks into _mocktails._ And honestly, the flavours really do taste like cocktails !

![](media/cafecoquetel.png)

## Venelle Café

A little newcomer this year on the Plateau, Venelle Café is what I would call the perfect space-efficient coffee shop. When you come in, the bricks walls and wood furnitures compliment a nice minimalist look that you can’t help but feel serene. Loving the plant’s touch as well.

![](media/venellecafe.png)

## Saison Des Pluies

I fortunately got the chance to visit this coffee shop before they forbid to sit inside. A very simple coffee shop de quartier, with a nice little terrace in the summer time.

![](media/saisondespluies.JPG)

## Café Chez Téta

This coffee shop has a big place in my heart because they nailed not only the hot chocolate, but also the food! With the Lebanese touch in their menus, you won’t regret visiting this coffee shop owned by the most adorable couple!

![](media/cafecheztetamtl.png)

## Café Chez Mère-Grand

**Of course I could not close this article without mentioning my all-time favourite coffee shop !** The sweetest owners and staff that I’m happy to befriend. They are always in their best attitude and put so much love and passion in reinventing their menus, the creativity is simply impressive to watch them go.

![](media/cafemeregrand.png)

{{% /content %}}

{{% float %}}

Coffee shops:

1. [Cafe Coquetel](https://www.cafecoquetel.com/) - Villeray
2. [Venelle Cafe ](https://www.cafecoquetel.com/)- Plateau
3. [Saison Des Pluies](https://www.instagram.com/saisondespluies.cafe/?hl=en) - Villeray
4. [Cafe Chez Teta](https://www.google.com/search?q=cafe+chez+teta&rlz=1C5GCEA_enCA879CA888&oq=cafe+chez+teta&aqs=chrome..69i57j35i39j0.3019j0j4&sourceid=chrome&ie=UTF-8) - Plateau
5. [Cafe Chez Mère-Grand](https://meregrand.ca/) - Old-Port of MTL

{{% /float %}}
