// ISOTOPE -------------------------------------------------------------------
// See [Masonry](https://isotope.metafizzy.co/)

// Initialize masonry container
var iso = new Isotope(document.querySelector('.iso'), {
  itemSelector: '.iso-item',
  percentPosition: true,
  masonry: {
    columnWidth: '.iso-sizer',
    gutter: 10}});

// Delay generating layout until images are lazily loaded
imagesLoaded(
  document.querySelector('.iso'))
  .on('progress',
    () => {
      iso.layout()});


