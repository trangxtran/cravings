"use strict";
// NAVBAR --------------------------------------------------------------------
// Show navbar after scrolling pass the fold

const fade = (id) => () => {
  let nav = document.getElementById(id);
  if (window.scrollY > 0)
    nav.classList.add("invis");
  else
    nav.classList.remove("invis")}
